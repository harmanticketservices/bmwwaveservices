angular.module('ramWebSevice')
 .controller('crashIdsController', ['$scope', '$http', '$routeParams', '$timeout', 'growl', 'TicketService', function($scope, $http, $routeParams, $timeout, growl, TicketService) {

   
	$scope.resetValues = function() {
        $scope.crashIdInfo = undefined;
        $scope.crashId = undefined;
        $scope.dataRetrieved = false;
    }

    $scope.getCrashInfo = function() {
    	$http.get("/crash_ids/" + $scope.crashId + "/get_info/", {}).then(function(response) {
            $scope.crashIdInfo = response.data;
            $scope.dataRetrieved = true;
        }).catch(function(err) {
            console.log(err);
            $scope.resetValues();
        });
    }

    $scope.resetValues();

    $scope.crashId = $routeParams.crash_id;

    if (typeof $scope.crashId !== 'undefined') {
        $scope.getCrashInfo();
    }
}]);