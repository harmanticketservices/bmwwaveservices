angular.module('ramWebSevice').
controller('chartsController', ['$scope', '$http', '$routeParams', '$timeout', 'growl', 'TicketService', '$sce', function($scope, $http, $routeParams, $timeout, growl, TicketService, $sce) {

    $scope.chartList = [];

    $scope.chartTemplatesMap = {
        'Ticket by Status': {
            "$and": [{
                "generalInfo.Status": {
                    "$in": ["Categorizing", "Reproduction", "Processing", "PreIntegrating", "Integrating", "Verifying", "Concluding", "Closed"]
                }
            }]
        },
        'Tickets by Stability Type': {
            "$and": [{
                "generalInfo.Domain": {
                    "$in": ["Stability", "Hang", "OutOfMemory", "Performance", "Reset"]
                }
            }]
        },
        'Ticket Duration': {
            "$and": [{
                "generalInfo.Age": {
                    "$gt": 0
                }
            }]
        },
        'Custom': null
    };

    $scope.chartTemplatesMapKeys = Object.keys($scope.chartTemplatesMap);
    $scope.selectedChartTemplate = { value: $scope.chartTemplatesMapKeys[0] };
    $scope.lastMongoQuery = null;
    $scope.chartTypes = ['Bar', 'Pie'];
    $scope.chartOrientations = ['h', 'v'];

    $scope.titleCheck = function(title) {
        if (title || $scope.selectedChartTemplate.value != 'Custom') {
            $scope.addChart();
        } else {
            growl.addErrorMessage("Please insert a chart title.");
        }
    }

    $scope.addChart = function() {
        if ($scope.selectedChartTemplate.value === 'Custom') {
            $scope.lastMongoQuery = $('#builder').queryBuilder('getMongo');
        } else {
            $scope.lastMongoQuery = $scope.chartTemplatesMap[$scope.selectedChartTemplate.value];
        }

        $http.post('/charts/add', {
            "rawQuery": $scope.lastMongoQuery,
            "selectedChartTemplate": $scope.selectedChartTemplate.value,
            "chartType": $scope.chartType,
            "chartTitle": $scope.chartTitle,
            "chartOrientation": $scope.chartOrientation
        }).then(function(response) {
            if (response) {
                if (response.data.status) {
                    $scope.chartList.push($sce.trustAsHtml(response.data.chart));
                    growl.addInfoMessage("Added a new chart!");
                } else {
                    growl.addErrorMessage(response.data.errorMessage);
                }
            }
        }).catch(function(err) {
            console.log(err);
            growl.addErrorMessage("Something went wrong.");
        });
    }

    $('#builder').queryBuilder({
        plugins: [
            'not-group',
            'sortable',
            'unique-filter',
            'bt-checkbox',
            'invert'
        ],
        filters: [{
            id: 'generalInfo.Priority',
            label: 'Priority',
            type: 'string',
            operators: ['equal', 'not_equal']
        }, {
            id: 'generalInfo.Category',
            label: 'Category',
            type: 'string',
            input: 'checkbox',
            values: {
                'Stability': 'Stability',
                'Hang': 'Hang',
                'OutOfMemory': 'OutOfMemory',
                'Performance': 'Performance',
                'Reset': 'Reset'
            },
            operators: ['in', 'not_in']
        }, {
            id: 'generalInfo.Domain',
            label: 'Domain',
            type: 'string',
            input: 'checkbox',
            values: {
                'Stability': 'Stability',
                'Housekeeping': 'Housekeeping',
                'Audio': 'Audio',
                'System': 'System',
                'Tuner': 'Tuner',
            },
            operators: ['in', 'not_in']
        }, {
            id: 'generalInfo.Responsible',
            label: 'Responsible',
            type: 'string',
            operators: ['equal', 'not_equal']
        }, {
            id: 'generalInfo.Planned',
            label: 'Planned',
            type: 'double',
            operators: ['between', 'in', 'not_in']
        }, {
            id: 'generalInfo.Status',
            label: 'Status',
            type: 'string',
            input: 'checkbox',
            values: {
                'Categorizing': 'Categorizing',
                'Reproduction': 'Reproduction',
                'Processing': 'Processing',
                'PreIntegrating': 'PreIntegrating',
                'Integrating': 'Integrating',
                'Verifying': 'Verifying',
                'Concluding': 'Concluding',
                'Closed': 'Closed'
            },
            operators: ['in', 'not_in']
        }, {
            id: 'generalInfo.Reported',
            label: 'Reported Date',
            type: 'date',
            validation: {
                format: 'YYYY/MM/DD'
            },
            plugin: 'datepicker',
            plugin_config: {
                format: 'yyyy/mm/dd',
                todayBtn: 'linked',
                todayHighlight: true,
                autoclose: true
            }
        }, {
            id: 'generalInfo.Categorizing',
            label: 'Categorizing Date',
            type: 'date',
            validation: {
                format: 'YYYY/MM/DD'
            },
            plugin: 'datepicker',
            plugin_config: {
                format: 'yyyy/mm/dd',
                todayBtn: 'linked',
                todayHighlight: true,
                autoclose: true
            }
        }, {
            id: 'generalInfo.Processing',
            label: 'Processing Date',
            type: 'date',
            validation: {
                format: 'YYYY/MM/DD'
            },
            plugin: 'datepicker',
            plugin_config: {
                format: 'yyyy/mm/dd',
                todayBtn: 'linked',
                todayHighlight: true,
                autoclose: true
            }
        }, {
            id: 'generalInfo.Age',
            label: 'Age',
            type: 'double',
            operators: ['between', 'in', 'not_in', 'greater', 'greater_or_equal', 'less', 'less_or_equal']
        }, {
            id: 'generalInfo.Developer',
            label: 'Developer',
            type: 'string',
            operators: ['equal', 'not_equal', 'contains']
        }, ]
    });
}]);