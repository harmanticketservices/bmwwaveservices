angular.module('ramWebSevice').controller('allCoreDumpJobController', ['$scope', '$http', 'CoredumpsService', 'growl', function($scope, $http, CoredumpsService, growl) {

    $scope.coreDumpJobs = [];
    $scope.filteredCoreDumpJobs = [];
    $scope.currentPage = 1;
    $scope.numPerPage = 2;
    $scope.maxSize = 5;

    CoredumpsService.getAllcoredumpJobIds().then(function(response) {
        $scope.coreDumpJobs = response.slice().reverse();
        $scope.numPerPage = 15;
        // growl.addSuccessMessage("Info retrieved!");
    }).catch(function(err) {
        console.log(err);
    });

    $scope.$watch('currentPage + numPerPage', function() {
        var begin = (($scope.currentPage - 1) * $scope.numPerPage);
        var end = begin + $scope.numPerPage;
        $scope.filteredCoreDumpJobs = $scope.coreDumpJobs.slice(begin, end);
    });
}]);
