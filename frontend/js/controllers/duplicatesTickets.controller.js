angular.module('ramWebSevice').controller('duplicatesTicketController',  ['$scope', '$http', 'growl', 'TicketService', function($scope, $http, growl, TicketService) {

    $scope.resetValues = function() {
        $scope.dupJobInProgress = false;
        $scope.ticketId = "";
        $scope.dupJobFinished = false;
        $scope.dups = [];
    }

    $scope.resetValues();

    $scope.searchDuplicatesForTicket = function() {
        if ((!isNaN($scope.ticketId)) && (typeof $scope.ticketId !== 'undefined') && ($scope.ticketId.toString().length === 7)) {
            $scope.dupJobInProgress = true;
            TicketService.getDuplicatesForTicket($scope.ticketId).then(function(response) {
                $scope.dups = response;
                $scope.dupJobFinished = true;
                $scope.dupJobInProgress = false;
                growl.addInfoMessage("Duplicate search done!");
            }).catch(function(err) {
                growl.addErrorMessage(err.data.message);
                $scope.resetValues();
            });
        } else {
            growl.addErrorMessage("Enter a valid elvis ticket");
            $scope.resetValues();
        }
    }
}]);
