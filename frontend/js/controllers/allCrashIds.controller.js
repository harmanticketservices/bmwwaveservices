angular.module('ramWebSevice').controller('allCrashIdsController', ['$scope', '$http', 'TicketService', 'growl', function($scope, $http, TicketService, growl) {

    $scope.crashIds = [];
    $scope.filteredCrashes = [];
    $scope.currentPage = 1;
    $scope.numPerPage = 2;
    $scope.maxSize = 5;

    $http.get("/crash_ids/get_all", {}).then(function(response) {
        $scope.crashIds = response.data;
        $scope.numPerPage = 15;
        // growl.addSuccessMessage("Info retrieved!");
    }).catch(function(err) {
        console.log(err);
    });

    $scope.$watch('currentPage + numPerPage', function() {
        var begin = (($scope.currentPage - 1) * $scope.numPerPage);
        var end = begin + $scope.numPerPage;
        $scope.filteredCrashes = $scope.crashIds.slice(begin, end);
    });
}]);
