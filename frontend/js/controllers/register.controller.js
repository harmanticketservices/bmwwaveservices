angular.module('ramWebSevice').controller('registerController', ['$scope', '$location', 'AuthService', function($scope, $location, AuthService) {

    $scope.register = function() {

        // initial values
        $scope.error = false;
        $scope.disabled = true;

        // call register from service
        AuthService.register($scope.registerForm.name,
                $scope.registerForm.email,
                $scope.registerForm.password)
            // handle success
            .then(function() {
                $location.path('/login');
                $scope.disabled = false;
                $scope.registerForm = {};
            })
            // handle error
            .catch(function(data) {
                $scope.error = true;
                $scope.errorMessage = "User already exists!";
                $scope.disabled = false;
                $scope.registerForm = {};
            });
    };
}]);
