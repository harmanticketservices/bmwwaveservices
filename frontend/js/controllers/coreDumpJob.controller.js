angular.module('ramWebSevice').controller('coreDumpJobController', ['$scope', '$http', 'CoredumpsService', 'growl', 'filterFilter', '$routeParams', 'ConstantsService',
    function($scope, $http, CoredumpsService, growl, filterFilter, $routeParams, ConstantsService) {

    $scope.resetValues = function() {
        $scope.variants = ConstantsService.getVariants();
        $scope.possibleProcessesPaths = ConstantsService.getProcessesPaths();
        $scope.variant = {};
        $scope.processFullPath = "";
        $scope.buildVersion = "";
        $scope.checkbox = {};
        $scope.checkbox.getExtendedBacktrace = false;
        $scope.coredumpsService = CoredumpsService;
        $scope.coredumpJobId = "";
        $scope.coredumpInfo = {};
        $scope.coredumpInfo.isFinished = false;
    }
   
    $scope.resetValues();

    $scope.getCoredumpJobData = function() {
        $scope.coredumpsService.getCoredumpJobData($scope.coredumpJobId).then(function(response) {
            $scope.coredumpInfo = response;
        }).catch(function(err) {
            console.log(err);
        });
    }

    $scope.coredumpJobId = $routeParams.core_dump_job_id;

    if (typeof $scope.coredumpJobId !== 'undefined') {
        $scope.getCoredumpJobData();
    }

    $scope.isFileAnArchive = function(file) {
        var arr = file.name.split(".");
        var ext = arr[arr.length - 1];
    }

     $scope.generateUID = function() {
        // I generate the UID from two parts here 
        // to ensure the random number provide enough bits.
        var firstPart = (Math.random() * 46656) | 0;
        var secondPart = (Math.random() * 46656) | 0;
        firstPart = ("000" + firstPart.toString(36)).slice(-3);
        secondPart = ("000" + secondPart.toString(36)).slice(-3);
        return firstPart + secondPart;
    }

    $scope.uploadFile = function() {
        if ($scope.buildVersion !== "" 
        && $scope.processFullPath !== "" 
        && typeof $scope.myFile !== 'undefined'
        ) {
            $scope.coredumpJobId = "CoreDumpJob_" + $scope.generateUID();
            growl.addInfoMessage("You'll get a notification when is ready..");
            $scope.coredumpsService.solveCoredumpJobAndNotify(
                $scope.coredumpJobId, 
                $scope.myFile, 
                { buildVersion: $scope.buildVersion, getExtendedBacktrace: $scope.checkbox.getExtendedBacktrace, processFullPath: $scope.processFullPath, variant: $scope.variant.value });
        } else {
             growl.addErrorMessage("Fill all the necessary fields!");
        }
    }

    $scope.uploadDeveloperFile = function(){
    if(typeof $scope.devFile !== 'undefined'){

        $scope.coredumpsService.uploadDevFile($scope.devFile,{devfilepath: $scope.devfilepath,devfile: $scope.devFile, buildVersion: $scope.buildVersion});
        growl.addInfoMessage("Please Wait until Build Info is Checked for your user")

    }
    else{
            growl.addInfoMessage("Please fill all the necessary fields");
        }
    }


    $scope.addServerBranch = function(){
        if($scope.branchPath !== ""){
            growl.addInfoMessage("Adding server branch")
            $scope.coredumpsService.addServerBranch($scope.branchPath)
        }
        else{
            growl.addErrorMessage("Fill the necessary fields!");
        }






    }
    $scope.solveAgain = function() {
        if ($scope.buildVersion !== "" 
        && $scope.processFullPath !== "" ) {
            growl.addInfoMessage("You'll get a notification when coredump has been extracted..");
            $scope.coredumpsService.solveCoredumpJobAgainAndNotify(
                $scope.coredumpJobId, 
                { buildVersion: $scope.buildVersion, getExtendedBacktrace: $scope.checkbox.getExtendedBacktrace, processFullPath: $scope.processFullPath, variant: $scope.variant.value });
        } else {
             growl.addErrorMessage("Fill all the necessary fields!");
        }
    }

    $scope.downloadCoreDump = function() {
        window.open("/coredumps/" + $scope.coredumpJobId + "/download_file/", "_blank", "");
    }

    $scope.$watch(function() {
        return $scope.coredumpsService.isSolvingCrashFinished($scope.coredumpJobId);
    }, function(isFinished) {
        if (isFinished === true) {
            $scope.getCoredumpJobData();
        }
    });


}]);
