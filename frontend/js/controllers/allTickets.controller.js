angular.module('ramWebSevice').controller('allTicketsController', ['$scope', '$http', 'TicketService', 'growl', function($scope, $http, TicketService, growl) {

    $scope.tickets = [];
    $scope.filteredTickets = [];
    $scope.currentPage = 1;
    $scope.numPerPage = 2;
    $scope.maxSize = 5;

    TicketService.getAllTicketIds().then(function(response) {
        $scope.tickets = response;
        $scope.numPerPage = 15;
        // growl.addSuccessMessage("Info retrieved!");
    }).catch(function(err) {
        console.log(err);
    });

    $scope.$watch('currentPage + numPerPage', function() {
        var begin = (($scope.currentPage - 1) * $scope.numPerPage);
        var end = begin + $scope.numPerPage;
        $scope.filteredTickets = $scope.tickets.slice(begin, end);
    });
}]);
