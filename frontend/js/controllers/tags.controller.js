angular.module('ramWebSevice').controller('tagsController', ['$scope', '$http', 'growl', '$routeParams', 'TagsService', function($scope, $http, growl, $routeParams, TagsService) {

    $scope.resetValues = function() {
        $scope.tagJobInProgress = false;
        $scope.tagJobFinished = false;
        $scope.tickets = [];
        $scope.tag = undefined;
    }

    $scope.resetValues();

    TagsService.getAllTags().then(function(tags) {
        $scope.allTags = tags;
    }).catch(function(err) {
        console.log(err);
    });

    $scope.searchByTag = function() {
        if ((typeof $scope.tag !== 'undefined') && ($scope.tag.length > 0)) {
            $scope.tagJobInProgress = true;
            TagsService.getAllTicketsByTag($scope.tag).then(function(response) {
                $scope.tickets = response;
                $scope.tagJobFinished = true;
                $scope.tagJobInProgress = false;
                growl.addInfoMessage("Search done!");
            }).catch(function(err) {
                growl.addErrorMessage(err.data.message);
                $scope.resetValues();
            });
        } else {
            growl.addErrorMessage("Enter a valid tag");
            $scope.resetValues();
        }
    }

    $scope.tag = $routeParams.tag;
    if (typeof $scope.tag !== 'undefined') {
        $scope.searchByTag();
    }
}]);
