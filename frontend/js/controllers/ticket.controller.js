angular.module('ramWebSevice')
    .controller('ticketController', ['$scope', '$http', '$routeParams', '$timeout', 'growl', 'TicketService', '$location', '$q', 'filterFilter', 'TagsService', 'ConstantsService',
        function($scope, $http, $routeParams, $timeout, growl, TicketService, $location, $q, filterFilter, TagsService, ConstantsService) {

            $scope.resetValues = function() {
                $scope.variants = ConstantsService.getVariants();
                $scope.possibleProcessesPaths = ConstantsService.getProcessesPaths();
                $scope.ticketStatus = "";
                $scope.ticketId = undefined;
                $scope.elvisInfoRetrieved = false;
                $scope.ticketInfo = undefined;
                $scope.checkbox = {};
                $scope.checkbox.getExtendedBacktrace = false;
                $scope.manualPath = {};
                $scope.manualPath.processFullPath = "";
                $scope.ticketService = TicketService;
                $scope.tagsService = TagsService;
                $scope.editableTicketTags = [];
                $scope.variant = {};
            }

            $scope.getTicketInfo = function() {
                if ((!isNaN($scope.ticketId)) && (typeof $scope.ticketId !== 'undefined') && ($scope.ticketId.toString().length === 7)) {
                    $scope.ticketService.getTicketData($scope.ticketId).then(function(response) {
                        $scope.ticketInfo = response;
                        if (typeof response.tags !== 'undefined') {
                            for (i = 0; i < response.tags.length; ++i) {
                                var tag = response.tags[i];
                                if ($scope.editableTicketTags.indexOf(tag) === -1) {
                                    $scope.editableTicketTags.push(tag);
                                }
                            }
                        }
                        $scope.elvisInfoRetrieved = true;
                        growl.addSuccessMessage("Elvis info retrieved for ticket " + $scope.ticketId + " !");
                    }).catch(function(err) {
                        $scope.resetValues();
                        console.log(err);
                        $location.path('/');
                    });
                } else {
                    growl.addErrorMessage("Enter a valid elvis ticket");
                    $scope.resetValues();
                }
            }

            $scope.isValidLinuxPath = function(path) {
                var re = new RegExp("^(/[^/ ]*)+/?$");
                return re.test(path);
            }

            $scope.solveCrashes = function() {
                var isPathTypedByUser = (typeof $scope.manualPath.processFullPath !== "undefined" && $scope.manualPath.processFullPath !== "");
                var isValidPath = true; // = $scope.isValidLinuxPath($scope.manualPath.processFullPath);
                if (isPathTypedByUser && !isValidPath) {
                    $scope.manualPath.processFullPath = "";
                    growl.addErrorMessage("Enter a valid process path");
                    return;
                }

                growl.addInfoMessage("You'll get a notification when is ready..");
                var modifiedPath = $scope.manualPath.processFullPath.replace(/\\/g, '!');
                $scope.ticketService.solveCrashesForTicketAndNotify(
                    $scope.ticketId, { getExtendedBacktrace: $scope.checkbox.getExtendedBacktrace, processFullPath: modifiedPath, variant: $scope.variant.value });
            }

            $scope.solveCrashesAgain = function() {
                var isPathTypedByUser = (typeof $scope.manualPath.processFullPath !== "undefined" && $scope.manualPath.processFullPath !== "");
                var isValidPath = true; // = $scope.isValidLinuxPath($scope.manualPath.processFullPath);
                if (isPathTypedByUser && !isValidPath) {
                    $scope.manualPath.processFullPath = "";
                    growl.addErrorMessage("Enter a valid process path");
                    return;
                }

                $scope.ticketInfo.areBacktracesSolved = false;
                growl.addInfoMessage("You'll get a notification when is ready..");
                var modifiedPath = $scope.manualPath.processFullPath.replace(/\\/g, '!');
                $scope.ticketService.solveCrashesForTicketAgainAndNotify(
                    $scope.ticketId, { getExtendedBacktrace: $scope.checkbox.getExtendedBacktrace, processFullPath: modifiedPath, variant: $scope.variant.value });
                $scope.ticketInfo.solvedCrashes = undefined;
                $scope.editableTicketTags = [];
            }

            $scope.$watch(function() {
                return $scope.ticketService.isSolvingCrashFinished($scope.ticketId);
            }, function(isFinished) {
                if (isFinished === true) {
                    $scope.getTicketInfo();
                }
            });

            $scope.$watch(function() {
                return $scope.ticketService.getTicketStatus($scope.ticketId);
            }, function(newTicketStatus) {
                if (typeof newTicketStatus !== 'undefined') {
                    $scope.ticketStatus = newTicketStatus;
                }
            });

            $scope.growlCopyClipboard = function() {
                growl.addInfoMessage("Copied to clipboard");
            }

            $scope.downloadBacktraceLogs = function() {
                window.open("/backtraces/" + $scope.ticketId + "/download_logs/", '_blank', '');
            }

            $scope.downloadCoreDump = function(coreDumpPath) {
                var filename = coreDumpPath.split("/").pop().split(".")[0];
                window.open("/backtraces/" + $scope.ticketId + "/" + filename + "/download/", "_blank", "");
            }

            $scope.downloadKernelPanicCoreDump = function() {
                window.open("/backtraces/" + $scope.ticketId + "/kernel-panic/download/", "_blank", "");
            }

            $scope.resetValues();

            $scope.ticketId = $routeParams.ticket_id;

            if (typeof $scope.ticketId !== 'undefined') {
                $scope.tagsService.getAllTags().then(function(tags) {
                    $scope.ticketTags = tags;
                }).catch(function(err) {
                    console.log(err);
                });

                $scope.getTicketInfo();
            }

            function querySearchDeferred(query) {
                var deferred = $q.defer();
                setTimeout(function() {
                    if (query) {
                        deferred.resolve(filterFilter($scope.ticketTags, query));
                    } else {
                        deferred.reject(['None']);
                    }
                }, 200);
                return deferred.promise;
            }

            $scope.querySearchDeferred = querySearchDeferred;

            $scope.saveTags = function() {
                $scope.ticketService.changeTagsForTicket($scope.ticketId, $scope.editableTicketTags).then(function(response) {
                    growl.addSuccessMessage("Tags changed successfully!");
                }).catch(function(err) {
                    $scope.resetValues();
                    growl.addErrorMessage(err.data.message);
                });
            }
        }
    ]);