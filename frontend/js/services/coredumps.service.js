angular.module('ramWebSevice').factory('CoredumpsService', ['$q', '$timeout', '$http', 'growl', 'FileUploadService',

    function($q, $timeout, $http, growl, FileUploadService) {

        var solvedJobsById = new Object();

        function getStatusForJob(coredumpJobId) {
            var deferred = $q.defer();
            $http.get("/coredumps/" + coredumpJobId + "/get_job_status/", {}).then(function(response) {
                deferred.resolve(response.data);
            }).catch(function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function poolJobsStatus(coredumpJobId) {
            $timeout(function() {
                getStatusForJob(coredumpJobId).then(function(response) {
                    if (response.isFinished === true) {
                        if (response.errorMessage !== "") {
                            growl.addErrorMessage("Processing for for <a href=\"/#!/core_dump_job/" + coredumpJobId + "\"><strong>" + coredumpJobId + "</strong></a> failed! Error: " + response.errorMessage, { ttl: -1 });
                        } else {
                            growl.addSuccessMessage("Processing for <a href=\"/#!/core_dump_job/" + coredumpJobId + "\"><strong>" + coredumpJobId + "</strong></a> is ready!", { ttl: -1 });
                        }
                        solvedJobsById[coredumpJobId] = true;
                    } else {
                        if (response.errorMessage !== "") {
                            solvedJobsById[coredumpJobId] = true;
                            growl.addErrorMessage("Processing for <a href=\"/#!/core_dump_job/" + coredumpJobId + "\"><strong>" + coredumpJobId + "</strong></a> failed! Error: " + response.errorMessage, { ttl: -1 });
                        }
                    }
                }).catch(function(err) {
                    growl.addErrorMessage(err);
                }).finally(function() {
                    if (solvedJobsById[coredumpJobId] === false) {
                        poolJobsStatus(coredumpJobId);
                    }
                });
            }, 2000);
        }

        function getCoredumpJobData(coredumpJobId) {
            var deferred = $q.defer();
            $http.get("/coredumps/" + coredumpJobId + "/get_info/", {}).then(function(response) {
                response.data.crash.payload = JSON.parse(response.data.crash.payload);
                response.data.crash.showCrash = false;
                response.data.crash.bt = response.data.crash.bt.replace(/[^\x20-\x7E\n]+/g, '');
                var arr = response.data.coredumpPath.split("/");
                response.data.crash.btName = "Backtrace_" + arr[arr.length - 1].split(".")[0] + ".txt";
                response.data.crash.btTxtBlob = (window.URL || window.webkitURL).createObjectURL(new Blob([response.data.crash.bt], { type: 'text/plain' }));
                deferred.resolve(response.data);
            }).catch(function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function solveCoredumpJob(coredumpJobId, file, options) {
            var deferred = $q.defer();

            var url = "/coredumps/" 
                + coredumpJobId + "/" 
                + options.variant + "/" 
                + btoa(options.processFullPath) + "/" 
                + options.buildVersion + "/" 
                + options.getExtendedBacktrace + "/add_job/";

            // console.log(url);
            // console.log(options);

            FileUploadService.uploadFileToUrl(file, url).then(function(response) {
                growl.addInfoMessage("COCOSBANANICA1");
                deferred.resolve(response.data);
            }).catch(function(err) {
                deferred.reject(err);
            });

            return deferred.promise;
        }

        function uploadDevFile(file,options){
            var deferred = $q.defer();
             var url = "/developer_core_dump_job/upload_file/" + btoa(options.devfilepath) + "/" + btoa(options.buildVersion)

            // console.log(url);
            // console.log(options);

            FileUploadService.uploadFileToUrl(file, url).then(function(response) {
                deferred.resolve(response.datal);
            }).catch(function(err) {
                deferred.reject(err);
            });

            return deferred.promise;
        }



        function solveCoredumpJobAgain(coredumpJobId, options) {
            var deferred = $q.defer();

            var url = "/coredumps/" 
                + coredumpJobId + "/" 
                + options.variant + "/" 
                + btoa(options.processFullPath) + "/" 
                + options.buildVersion + "/" 
                + options.getExtendedBacktrace + "/add_job_again/";

            $http.post(url).then(function(response) {
                deferred.resolve(response.data);
            }).catch(function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function getLogsForCoredumpJob(coredumpJobId) {
            var deferred = $q.defer();
            $http.get("/coredumps/" + coredumpJobId + "/download_logs/", {}).then(function(response) {
                deferred.resolve(response.data);
            }).catch(function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function isSolvingCrashInProgress(coredumpJobId) {
            return ((coredumpJobId in solvedJobsById) && (solvedJobsById[coredumpJobId] === false));
        }

        function isSolvingCrashFinished(coredumpJobId) {
            return ((coredumpJobId in solvedJobsById) && (solvedJobsById[coredumpJobId] === true));
        }

        function isUploadingFile(){
        var deferred = $q.defer();
        $http.get("/developer_core_dump_job/get_status",{}).then(function(response){
            deferred.resolve(response.data)
            })
        }

        function solveCoredumpJobAndNotify(coredumpJobId, file, options) {
            if (coredumpJobId in solvedJobsById && solvedJobsById[coredumpJobId] === false) {
                growl.addWarnMessage(coredumpJobId + " is already in progress");
            } else {
                getStatusForJob(coredumpJobId).then(function(response) {
                    if (response.status === "finished") {
                        if (response.errorMessage !== "") {
                            growl.addErrorMessage("Processing for <a href=\"/#!/core_dump_job/" + coredumpJobId + "\"><strong>" + coredumpJobId + "</strong></a> failed! Error: " + response.errorMessage, { ttl: -1 });
                        } else {
                            growl.addSuccessMessage("Processing for <a href=\"/#!/core_dump_job/" + coredumpJobId + "\"><strong>" + coredumpJobId + "</strong></a> is ready!", { ttl: -1 });
                        }
                        solvedJobsById[coredumpJobId] = true;
                    } else {
                        solveCoredumpJob(coredumpJobId, file, options).then(function(response) {
                            solvedJobsById[coredumpJobId] = false;
                            poolJobsStatus(coredumpJobId);
                        }).catch(function(err) {
                            console.log(err);
                        })
                    }
                }).catch(function(err) {
                    console.log(err);
                })
            }
        }

        function solveCoredumpJobAgainAndNotify(coredumpJobId, options) {
            solveCoredumpJobAgain(coredumpJobId, options).then(function(response) {
                solvedJobsById[coredumpJobId] = false;
                poolJobsStatus(coredumpJobId);
            }).catch(function(err) {
                console.log(err);
            })
        }

        function getAllcoredumpJobIds() {
            var deferred = $q.defer();
            $http.get("/coredumps/get_all/", {}).then(function(response) {
                deferred.resolve(response.data);
            }).catch(function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function addServerBranch(branchName){
           var deferred = $q.defer();
           var addServerBranchPostUrl = "/coredumps/" + btoa(branchName) + "/add_branch/";
           $http.post(addServerBranchPostUrl).then(function (response) {
                deffered.resolve(response);
            }).catch(function (response) {
                deffered.reject(response);
            });
            return deferred.promise;

        }

        // return available functions for use in controllers
        return ({
           getCoredumpJobData: getCoredumpJobData,
           solveCoredumpJobAndNotify: solveCoredumpJobAndNotify,
           solveCoredumpJobAgainAndNotify: solveCoredumpJobAgainAndNotify,
           getLogsForCoredumpJob: getLogsForCoredumpJob,
           getAllcoredumpJobIds: getAllcoredumpJobIds,
           isSolvingCrashFinished: isSolvingCrashFinished,
           isSolvingCrashInProgress: isSolvingCrashInProgress,
           uploadDevFile: uploadDevFile,
           isUploadingFile: isUploadingFile,
           addServerBranch: addServerBranch
        });
    }
]);
