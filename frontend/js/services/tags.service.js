angular.module('ramWebSevice').factory('TagsService', ['$q', '$http',

    function($q, $http) {
        function getAllTags() {
            var deferred = $q.defer();
            $http.get("/tags/get_all/", {}).then(function(response) {
                var tags = [];
                if (typeof response.data !== 'undefined') {
                    for (i = 0; i < response.data.length; ++i) {
                        tags.push(response.data[i].text);
                    }
                }
                deferred.resolve(tags);
            }).catch(function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function getAllTicketsByTag(tag) {
            var deferred = $q.defer();
            $http.post("/ticket/get_all_by_tag", {tag: tag}).then(function(response) {
                deferred.resolve(response.data);
            }).catch(function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        // return available functions for use in controllers
        return ({
            getAllTags: getAllTags,
            getAllTicketsByTag: getAllTicketsByTag,
        });
    }
]);
