angular.module('ramWebSevice').factory('ConstantsService', [function() {
        function getProcessesPaths() {
           var paths = ["/usr/bin/dlt-daemon",
                "/usr/bin/PathologyProc",
                "/usr/bin/dev-ioamp-router",
                "/usr/bin/System",
                "/home/tuneramfmdab/bin/TunerApp",
                "/usr/bin/RamDiagApp",
                "/usr/bin/someip_sddaemon",
                "/home/tunertv/bin/TunerAppTv",
                "/home/tunertv/bin/dev-tuner-tv",
                "/usr/bin/AudioProcess",
                "/home/tuneramfmdab/bin/dev-tuner-amfm",
                "/usr/bin/PingIPC",
                "/home/tunersdars/bin/TunerAppSdars",
                "/usr/bin/PIA",
                "/usr/bin/dlt-booster"];
            return paths;
        }

        function getVariants() {
            var variants = ['tbd'];
            return variants;
        }

        // return available functions for use in controllers
        return ({
            getProcessesPaths: getProcessesPaths,
            getVariants: getVariants,
        });
    }
]);
