angular.module('ramWebSevice').factory('AuthService', ['$q', '$timeout', '$http',
    function($q, $timeout, $http) {

        var userObj = {
          isLoggedIn: false,
          userName: null,
          userEmail: null,
          userRoles: null,
          isAdmin: false,
          isDeveloper: false

        };

        function isLoggedIn() {
            return userObj.isLoggedIn
        }

        function login(email, password) {

            // create a new instance of deferred
            var deferred = $q.defer();

            // send a post request to the server
            $http.post('/user/login', { email: email, password: password })
                // handle success
                .then(function(data) {
                    if(data.data.result === "dev"){
                        userObj.isLoggedIn = true;
                        userObj.userEmail=email;
                        userObj.isDeveloper=true;
                        deferred.resolve();
                    }
                    else if (data.data.result === "ok") {
                        userObj.isLoggedIn = true;
                        userObj.userEmail = email;
                        userObj.isDeveloper = false;


                        deferred.resolve();
                    }
                    else {

                        userObj.isLoggedIn = false;
                        deferred.reject();
                    }
                })
                // handle error
                .catch(function(data) {
                    userObj.isLoggedIn = false;
                    deferred.reject();
                });

            // return promise object
            return deferred.promise;

        }

        function logout() {

            // create a new instance of deferred
            var deferred = $q.defer();

            // send a get request to the server
            $http.get('/user/logout')
                // handle success
                .then(function(data) {
                    userObj.isLoggedIn = false;
                    deferred.resolve();
                })
                // handle error
                .catch(function(data) {
                    userObj.isLoggedIn = false;
                    deferred.reject();
                });

            // return promise object
            return deferred.promise;
        }

        function register(name, email, password) {
            // create a new instance of deferred
            var deferred = $q.defer();

            // send a post request to the server
            $http.post('/user/register', { name: name, email: email, password: password })
                .then(function(data) {
                    if (data.data.result === "ok") {
                        deferred.resolve();
                    } else {
                        deferred.reject();
                    }
                })
                // handle error
                .catch(function(data) {
                    deferred.reject();
                });

            // return promise object
            return deferred.promise;
        }

        function getUserStatus() {
            return $http.get('/user/status')
                // handle success
                .then(function(data) {
                    if (data.data.status === true) {
                        userObj.isLoggedIn = true;
                        userObj.userName = data.data.userName;
                        userObj.userEmail = data.data.userEmail;
                        userObj.userRoles = data.data.userRoles;

                    } else {
                        userObj.isLoggedIn = false;
                    }
                })
                // handle error
                .catch(function(data) {
                    userObj.isLoggedIn = false;
                });
        }

        function getUserEmail() {
            return userObj.userEmail;
        }

        function getUserName() {
            return userObj.userName;
        }

        function getUserRoles() {
            return userObj.userRoles;
        }

        function isAdmin() {
            if (userObj.userRoles) {
              var indexOfRole = userObj.userRoles.indexOf('admin');
              if (indexOfRole === -1)
                return false;
              else
                return true;
            }
        }

        function isDeveloper() {
            if (userObj.userRoles) {
              var indexOfRole = userObj.userRoles.indexOf('devuser');
              if (indexOfRole === -1)
                return false;
              else
                return true;
            }

        }


        // return available functions for use in controllers
        return ({
            getUserEmail: getUserEmail,
            getUserName: getUserName,
            getUserRoles: getUserRoles,
            isLoggedIn: isLoggedIn,
            isAdmin: isAdmin,
            login: login,
            logout: logout,
            register: register,
            getUserStatus: getUserStatus,
            isDeveloper: isDeveloper
        });

    }
]);
