angular.module('ramWebSevice').factory('TicketService', ['$q', '$timeout', '$http', '$timeout', '$localStorage', 'growl',

    function($q, $timeout, $http, $timeout, $localStorage, growl) {

        var statusMap = null;

        var ticketStatus = new Object();

        var solvedCrashesById = new Object();

        function saveInLocalStorage(ticketId) {
            var ticketNotifications = [];
            if ($localStorage.ticketNotifications) {
                ticketNotifications = $localStorage.ticketNotifications;
            }
            ticketNotifications.push(ticketId);
            $localStorage.ticketNotifications = ticketNotifications;
        }

        function removeFromLocalStorage(ticketId) {
            var ticketNotifications = [];
            if ($localStorage.ticketNotifications) {
                ticketNotifications = $localStorage.ticketNotifications;
            }
            var index = ticketNotifications.indexOf(ticketId);
            if (index >= 0) {
                ticketNotifications.splice(index, 1);
            }
            $localStorage.ticketNotifications = ticketNotifications;
        }

        function getStatusesMap() {
            $http.get('/get_status_map').then(function(response) {
                statusMap = response.data;
            }).catch(function(err) {
                console.log("Couldn't get statusMap from backend!");
            });
        }

        function getTicketStatus(ticketId) {
            return ticketStatus[ticketId];
        }

        function getCrashesStatusForTicket(ticketId) {
            var deferred = $q.defer();
            $http.get("/backtraces/" + ticketId + "/get_job_status/", {}).then(function(response) {
                deferred.resolve(response.data);
            }).catch(function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function poolCrashesStatus(ticketId) {
            $timeout(function() {
                getCrashesStatusForTicket(ticketId).then(function(response) {
                    if (response.status === "finished") {
                        if (response.errorMessage !== "") {
                            growl.addErrorMessage("Processing for for <a href=\"/#!/ticket/" + ticketId + "\"><strong>" + ticketId + "</strong></a> failed! Error: " + response.errorMessage, { ttl: -1 });
                        } else {
                            growl.addSuccessMessage("Processing for <a href=\"/#!/ticket/" + ticketId + "\"><strong>" + ticketId + "</strong></a> is ready!", { ttl: -1 });
                        }
                        solvedCrashesById[ticketId] = true;
                        removeFromLocalStorage(ticketId);
                    } else {
                        if (response.errorMessage !== "") {
                            solvedCrashesById[ticketId] = true;
                            removeFromLocalStorage(ticketId);
                            growl.addErrorMessage("Processing for <a href=\"/#!/ticket/" + ticketId + "\"><strong>" + ticketId + "</strong></a> failed! Error: " + response.errorMessage, { ttl: -1 });
                        }
                    }
                    ticketStatus[ticketId] = statusMap[response.status];
                }).catch(function(err) {
                    growl.addErrorMessage(err);
                    removeFromLocalStorage(ticketId);
                }).finally(function() {
                    if (solvedCrashesById[ticketId] === false) {
                        poolCrashesStatus(ticketId);
                    }
                });
            }, 2000);
        }

        function recoverFromLocalStorage() {
            var ticketNotifications = [];
            if ($localStorage.ticketNotifications) {
                ticketNotifications = $localStorage.ticketNotifications;
            }

            if (ticketNotifications.length === 0) {
                console.log("Nothing to recover from local storage");
            }

            for (i = 0; i < ticketNotifications.length; ++i) {
                var ticketId = ticketNotifications[i];
                if (!(ticketId in solvedCrashesById)) {
                    console.log("Recovering " + ticketId + " from local storage");
                    poolCrashesStatus(ticketId);
                }
            }
        }

        function getTicketData(ticketId) {
            var deferred = $q.defer();
            $http.get("/ticket/" + ticketId + "/get_info/", {}).then(function(response) {
                for (var i = 0; i < response.data.solvedCrashes.length; ++i) {
                    response.data.solvedCrashes[i].payload = JSON.parse(response.data.solvedCrashes[i].payload);
                    response.data.solvedCrashes[i].showCrash = false;
                    response.data.solvedCrashes[i].bt = response.data.solvedCrashes[i].bt.replace(/[^\x20-\x7E\n]+/g, '');

                    var arr = undefined;
                    if (typeof response.data.solvedCrashes[i].payload.core_path !== 'undefined') {
                        arr = response.data.solvedCrashes[i].payload.core_path.split("/");
                    } else {
                        arr = response.data.solvedCrashes[i].payload.crash_id.split("/");
                    }
                    response.data.solvedCrashes[i].btName = "Backtrace_" + arr[arr.length - 1].split(".")[0] + ".txt";

                    response.data.solvedCrashes[i].btTxtBlob = (window.URL || window.webkitURL).createObjectURL(new Blob([response.data.solvedCrashes[i].bt], { type: 'text/plain' }));
                }
                deferred.resolve(response.data);
            }).catch(function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function solveCrashesForTicket(ticketId, options) {
            var deferred = $q.defer();
            $http.post("/backtraces/" + ticketId + "/add_job/", options).then(function(response) {
                deferred.resolve(response.data);
                saveInLocalStorage(ticketId);
            }).catch(function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function solveCrashesForTicketAgain(ticketId, options) {
            var deferred = $q.defer();
            $http.post("/backtraces/" + ticketId + "/add_job_again/", options).then(function(response) {
                ticketStatus[ticketId] = "Pending to be processed..";
                deferred.resolve(response.data);
                saveInLocalStorage(ticketId);
            }).catch(function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function getBacktraceLogsForTicket(ticketId) {
            var deferred = $q.defer();
            $http.get("/backtraces/" + ticketId + "/download_logs/", {}).then(function(response) {
                deferred.resolve(response.data);
            }).catch(function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function isSolvingCrashInProgress(ticketId) {
            return ((ticketId in solvedCrashesById) && (solvedCrashesById[ticketId] === false));
        }

        function isSolvingCrashFinished(ticketId) {
            return ((ticketId in solvedCrashesById) && (solvedCrashesById[ticketId] === true));
        }

        function solveCrashesForTicketAndNotify(ticketId, options) {
            if (ticketId in solvedCrashesById && solvedCrashesById[ticketId] === false) {
                growl.addWarnMessage(ticketId + " is already in progress");
            } else {
                getCrashesStatusForTicket(ticketId).then(function(response) {
                    if (response.status === "finished") {
                        if (response.errorMessage !== "") {
                            growl.addErrorMessage("Processing for <a href=\"/#!/ticket/" + ticketId + "\"><strong>" + ticketId + "</strong></a> failed! Error: " + response.errorMessage, { ttl: -1 });
                        } else {
                            growl.addSuccessMessage("Processing for <a href=\"/#!/ticket/" + ticketId + "\"><strong>" + ticketId + "</strong></a> is ready!", { ttl: -1 });
                        }
                        solvedCrashesById[ticketId] = true;
                    } else {
                        solveCrashesForTicket(ticketId, options).then(function(response) {
                            solvedCrashesById[ticketId] = false;
                            poolCrashesStatus(ticketId);
                        }).catch(function(err) {
                            console.log(err);
                        })
                    }
                    ticketStatus[ticketId] = statusMap[response.status];
                }).catch(function(err) {
                    console.log(err);
                })
            }
        }

        function solveCrashesForTicketAgainAndNotify(ticketId, options) {
            solveCrashesForTicketAgain(ticketId, options).then(function(response) {
                solvedCrashesById[ticketId] = false;
                poolCrashesStatus(ticketId);
            }).catch(function(err) {
                console.log(err);
            })
        }

        function getDuplicatesForTicket(ticketId) {
            var deferred = $q.defer();
            $http.get("/ticket/" + ticketId + "/get_duplicates/", {}).then(function(response) {
                deferred.resolve(response.data);
            }).catch(function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function changeTagsForTicket(ticketId, tags) {
            var deferred = $q.defer();
            $http.post("/ticket/" + ticketId + "/change_tags/", tags).then(function(response) {
                deferred.resolve(response.data);
            }).catch(function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function getAllTicketIds() {
            var deferred = $q.defer();
            $http.get("/ticket/get_all", {}).then(function(response) {
                deferred.resolve(response.data);
            }).catch(function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        // return available functions for use in controllers
        return ({
            getStatusesMap: getStatusesMap,
            getTicketStatus: getTicketStatus,
            recoverFromLocalStorage: recoverFromLocalStorage,
            changeTagsForTicket: changeTagsForTicket,
            isSolvingCrashFinished: isSolvingCrashFinished,
            isSolvingCrashInProgress: isSolvingCrashInProgress,
            getBacktraceLogsForTicket: getBacktraceLogsForTicket,
            solveCrashesForTicketAgainAndNotify: solveCrashesForTicketAgainAndNotify,
            getTicketData: getTicketData,
            solveCrashesForTicket: solveCrashesForTicket,
            getCrashesStatusForTicket: getCrashesStatusForTicket,
            solveCrashesForTicketAndNotify: solveCrashesForTicketAndNotify,
            getDuplicatesForTicket: getDuplicatesForTicket,
            getAllTicketIds: getAllTicketIds
        });
    }
]);