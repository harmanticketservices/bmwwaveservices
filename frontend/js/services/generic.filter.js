angular.module('ramWebSevice.filters', []).filter('placeholder', [function() {
    return function(text, placeholder) {
        // If we're dealing with a function, get the value
        if (angular.isFunction(text)) text = text();
        // Trim any whitespace and show placeholder if no content
        if (typeof text === 'undefined')
        	return placeholder;
        return text.trim() || placeholder;
    };
}]);