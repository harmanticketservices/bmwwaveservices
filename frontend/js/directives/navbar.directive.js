angular.module('ramWebSevice')
    .directive('navbarDirective', ['$location', 'growl', 'AuthService', function($location, growl, AuthService) {
        return {
            restrict: 'E',
            templateUrl: 'frontend/pages/navbar.directive.html',
            link: function(scope, element, attrs) {
                scope.isLoggedIn = AuthService.isLoggedIn();
                scope.userEmail = AuthService.getUserEmail();
                scope.userRoles = AuthService.getUserRoles();

                scope.isAdmin = false;
                scope.isDeveloper = false;


                scope.$watch(function() {
                    return AuthService.isLoggedIn();
                }, function(value) {
                    scope.isLoggedIn = value;
                    scope.userEmail = AuthService.getUserEmail();
                    scope.userRoles = AuthService.getUserRoles();
                });

                scope.$watch(function() {
                    return AuthService.isAdmin();
                }, function(value) {
                    scope.isAdmin = value;
                });

                scope.$watch(function() {
                    return AuthService.isDeveloper();
                }, function(value) {
                    scope.isDeveloper = value;
                });

                scope.resetValues = function() {
                    scope.ticketId = "";
                }

                scope.resetValues();

                scope.getTicketInfo = function() {
                    if (scope.ticketId !== "") {
                        if ((!isNaN(scope.ticketId)) && (typeof scope.ticketId !== 'undefined') && (scope.ticketId.toString().length === 7)) {
                            $location.path('/ticket/' + scope.ticketId);
                        } else {
                            growl.addErrorMessage("Enter a valid elvis ticket");
                            scope.resetValues();
                        }
                        scope.resetValues();
                    }
                }
            },
            scope: {}
        };
    }]);
