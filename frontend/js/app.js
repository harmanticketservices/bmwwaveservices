angular.module('ramWebSevice', [
	'ngRoute',
	'ui.select',
	'ngSanitize',
	'ngMaterial',
	'ngMessages',
	'ngStorage',
	'ngAnimate',
	'angular-growl',
	'ui.bootstrap',
	'mgcrea.ngStrap',
	'ramWebSevice.filters',
]).config(['$routeProvider', function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl: '/frontend/pages/contact.html',
			access: {
				restricted: true
			}
		})
		.when('/login', {
			templateUrl: '/frontend/pages/login.html',
			controller: 'loginController',
			access: {
				restricted: false
			}
		})
		.when('/logout', {
			controller: 'logoutController',
			access: {
				restricted: true
			}
		})
		.when('/register', {
			templateUrl: '/frontend/pages/register.html',
			controller: 'registerController',
			access: {
				restricted: false
			}
		})
		.when('/tags/:tag?', {
			templateUrl: '/frontend/pages/tags.html',
			controller: 'tagsController',
			access: {
				restricted: true
			}
		})
		.when('/duplicates', {
			templateUrl: '/frontend/pages/duplicates.html',
			controller: 'duplicatesTicketController',
			access: {
				restricted: true
			}
		})
		.when('/contact', {
			templateUrl: '/frontend/pages/contact.html',
			controller: 'contactController',
			access: {
				restricted: true
			}
		})
		.when('/all_tickets', {
			templateUrl: '/frontend/pages/all_tickets.html',
			controller: 'allTicketsController',
			access: {
				restricted: true
			}
		})
		.when('/ticket/:ticket_id', {
			templateUrl: '/frontend/pages/ticket.html',
			controller: 'ticketController',
			access: {
				restricted: true
			}
		})
		.when('/crash/:crash_id', {
			templateUrl: '/frontend/pages/crash_ids.html',
			controller: 'crashIdsController',
			access: {
				restricted: true
			}
		})
		.when('/all_crashes', {
			templateUrl: '/frontend/pages/all_crash_ids.html',
			controller: 'allCrashIdsController',
			access: {
				restricted: true
			}
		})
		.when('/charts', {
			templateUrl: '/frontend/pages/charts.html',
			controller: 'chartsController',
			access: {
				restricted: true,
				permissions: 'devuser'
			},
		})
		.when('/core_dump_job/:core_dump_job_id?', {
			templateUrl: '/frontend/pages/core_dump_job.html',
			controller: 'coreDumpJobController',
			access: {
				restricted: true
			}
		})
		.when('/all_core_dump_jobs', {
			templateUrl: '/frontend/pages/all_core_dump_jobs.html',
			controller: 'allCoreDumpJobController',
			access: {
				restricted: true

			}
		})
		.when('/developer_core_dump_job', {
		    templateUrl:'/frontend/pages/developer_core_dump_job.html',
		    controller:'coreDumpJobController',
		    access: {
		        restricted: true,
                permissions: 'devuser'
		    }
		})
		.when('/AdminPage', {
		    templateUrl:'/frontend/pages/AdminPage.html',
		    controller:'coreDumpJobController',
		    access: {
		        restricted: true,
                permissions: 'admin'
		    }
		})

		.otherwise({
			redirectTo: '/',
			access: {
				restricted: false
			}
		});
}]).config(['growlProvider', function(growlProvider) {
	growlProvider.globalTimeToLive(3000);
	growlProvider.globalEnableHtml(true);
}]).config(['$compileProvider',
	function ($compileProvider) {
		$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|blob):/);
}]).run(['$rootScope', '$location', '$route', 'growl', 'AuthService', 'TicketService',
	function($rootScope, $location, $route, growl, AuthService, TicketService) {

		TicketService.recoverFromLocalStorage();

		TicketService.getStatusesMap();

		$rootScope.$on('$routeChangeStart', function(event, next, current) {
			AuthService.getUserStatus().then(function() {

				if (next.access.restricted && !AuthService.isLoggedIn()) {
					$location.path('/login');
					$route.reload();

				}
				else if (next.access.restricted && next.access.permissions === 'devuser' && AuthService.isLoggedIn()) {
					if (AuthService.getUserRoles().indexOf(next.access.permissions) === -1) {
						$location.path('/');
						$route.reload();
						growl.addErrorMessage("Devuser privileges required for that page!");
					}

				}

			})
		})
	}
]);
