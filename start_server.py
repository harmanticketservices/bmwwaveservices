from flask import Flask, send_file, request, jsonify, g
from flask_mail import Mail,  Message
from werkzeug import secure_filename
from flask_security import login_required, roles_required, roles_accepted, current_user
import os, sys

#command for mount: sudo mount -t cifs //HIMDWSFS11/BMW_Wave/20_Software-Integration /media/share/ -o username=AlRadu
#username is debatable, based on the user. Access to the folder required on Windows

sys.path.append(os.path.join(os.path.dirname(__file__), "backend", "BacktraceExtractor"))
sys.path.append(os.path.join(os.path.dirname(__file__), "backend", "BacktraceExtractor", "CoreDumpJob"))
sys.path.append(os.path.join(os.path.dirname(__file__), "backend", "BacktraceExtractor", "ElvisTicketJob"))
sys.path.append(os.path.join(os.path.dirname(__file__), "backend", "Misc"))
sys.path.append(os.path.join(os.path.dirname(__file__), "backend", "Config"))
sys.path.append(os.path.join(os.path.dirname(__file__), "backend", "ElvisTicketDownloader"))
sys.path.append(os.path.join(os.path.dirname(__file__), "backend", "MongoDbModels"))
sys.path.append(os.path.join(os.path.dirname(__file__), "backend", "MongoDbModels", "CoreDumpJob"))
sys.path.append(os.path.join(os.path.dirname(__file__), "backend", "MongoDbModels", "ElvisTicketJob"))
sys.path.append(os.path.join(os.path.dirname(__file__), "backend", "MongoDbRepos"))
sys.path.append(os.path.join(os.path.dirname(__file__), "backend", "MongoDbRepos", "CoreDumpJob"))
sys.path.append(os.path.join(os.path.dirname(__file__), "backend", "MongoDbRepos", "ElvisTicketJob"))
sys.path.append(os.path.join(os.path.dirname(__file__), "backend", "Plotting"))

from AppInitializer import AppInitializer
from WebException import WebException
from ElvisTicketProcessorManager import ElvisTicketProcessorManager
from CoredumpsJobProcessorManager import CoredumpsJobProcessorManager
from CoredumpRepo import CoredumpRepo
from TicketRepo import TicketRepo, STATUS_AND_RESPONSE_MAP
from UserRepo import UserRepo
from CrashRepo import CrashRepo
from TagRepo import TagRepo
from Globals import *
from TicketDownloaderConfig import *
from ChartManager import ChartManager

app = Flask(__name__, static_folder = os.path.join(os.path.dirname(__file__), "frontend"))

# create working dirs and make other configs
AppInitializer.Instance().InitApp(app)

@app.route("/")
def index():
    return send_file("frontend/index.html")

@app.route('/favicon.ico')
def favicon():
    return send_file("frontend/favicon.ico")

@app.errorhandler(WebException)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response



@app.route("/coredumps/<branch_name>/add_branch/", methods = ['POST'])
def addServerBranch(branch_name):
    return CoredumpsJobProcessorManager.Instance().addServerBranch(branch_name)


@app.route("/backtraces/<ticket_id>/add_job/", methods = ['POST'])
def addBacktraceJob(ticket_id):
    extendedBacktrace = False
    if "getExtendedBacktrace" in request.get_json(silent=True):
        extendedBacktrace = request.get_json(silent=True)['getExtendedBacktrace']
    processFullPath = ""
    if "processFullPath" in request.get_json(silent=True):
        processFullPath = request.get_json(silent=True)['processFullPath']
    variant = ""
    if "variant" in request.get_json(silent=True):
        variant = request.get_json(silent=True)['variant']
    return ElvisTicketProcessorManager.Instance().addJob(ticket_id, extendedBacktrace, processFullPath, variant)

@app.route("/backtraces/<ticket_id>/add_job_again/", methods = ['POST'])
def addBacktraceJobAgain(ticket_id):
    extendedBacktrace = False
    if "getExtendedBacktrace" in request.get_json(silent=True):
        extendedBacktrace = request.get_json(silent=True)['getExtendedBacktrace']
    processFullPath = ""
    if "processFullPath" in request.get_json(silent=True):
        processFullPath = request.get_json(silent=True)['processFullPath']
        variant = ""
    if "variant" in request.get_json(silent=True):
        variant = request.get_json(silent=True)['variant']
    return ElvisTicketProcessorManager.Instance().addJobAgain(ticket_id, extendedBacktrace, processFullPath, variant)

@app.route("/backtraces/<ticket_id>/get_job_status/", methods = ['GET'])
def getBacktraceJobStatus(ticket_id):
    return TicketRepo.Instance().getProcessingStatus(ticket_id)

@app.route("/backtraces/<ticket_id>/download_logs/", methods = ['GET', 'POST'])
def downloadLogs(ticket_id):
    logPath = os.path.join(download_root, ticket_id, "btExtractorTmp", "backtraceExtractor.log")
    if os.path.isfile(logPath):
        return send_file(logPath, attachment_filename="{ticket_id}_processing.log".format(ticket_id=ticket_id), as_attachment=True)
    else:
        raise WebException(message="Backtrace log doesn't exist for {ticketId}!".format(ticketId = ticket_id), status_code=401)

@app.route("/backtraces/<ticket_id>/<core_dump>/download/", methods = ['GET'])
def downloadCoreDump(ticket_id, core_dump):
    archivePath = ""
    try:
        archivePath =  ElvisTicketProcessorManager.Instance().downloadCoreDump(ticket_id, core_dump)
    except Exception as e:
        raise WebException(message="Core dump could not be extracted from the dlt file", status_code=401)

    return send_file(archivePath, as_attachment=True)

@app.route("/get_status_map", methods = ['GET'])
def get_status_map():
    return jsonify(STATUS_AND_RESPONSE_MAP)

@app.route("/ticket/<ticket_id>/change_tags/", methods=['POST'])
def changeTags(ticket_id):
    tags = request.get_json(silent=True)
    return TicketRepo.Instance().changeTags(ticket_id, tags)

@app.route("/ticket/<ticket_id>/get_duplicates/", methods = ['GET'])
def getDuplicatesForTicket(ticket_id):
    return TicketRepo.Instance().getDuplicateTicketsFor(ticket_id)

@app.route("/ticket/<ticket_id>/get_info/", methods = ['GET'])
def getTicketInfo(ticket_id):
    res = TicketRepo.Instance().getTicketInfo(ticket_id) 
    return res.to_json()

@app.route("/ticket/get_all/", methods = ['GET'])
def getAllTickets():
    res = TicketRepo.Instance().getAllTicketsIds() 
    return res.to_json()

@app.route("/ticket/get_all_by_tag", methods=['POST'])
def getAllByTag():
    tag = request.get_json(silent=True)['tag']
    return TicketRepo.Instance().getAllByTag(tag)

@app.route("/crash_ids/<crash_id>/get_info/", methods = ['GET'])
def getCrashIdInfo(crash_id):
    res = CrashRepo.Instance().findByCrashId(crash_id)
    return res.to_json()

@app.route("/crash_ids/get_all/", methods = ['GET'])
def getAllCrashes():
    res = CrashRepo.Instance().getAllCrahesIds()
    return res.to_json()

@app.route("/tags/get_all/", methods = ['GET'])
@roles_required('devuser')
def getAllTags():
    res = TagRepo.Instance().getAllTags()
    return res.to_json()

@app.route('/user/register', methods=['POST'])
def register():
    json_data = request.json
    return UserRepo.Instance().addUser(json_data['name'], json_data['email'], json_data['password'])

@app.route('/user/login', methods=['POST'])
def login():
    json_data = request.json

    result = UserRepo.Instance().authenticateUser(json_data['email'], json_data['password'])
    isDev=UserRepo.Instance().checkDeveloper(json_data['email'])
    if(isDev):
        sandboxCreated=CoredumpsJobProcessorManager.Instance().checkDeveloperSandbox(json_data['email'])


    if result == True and isDev == True :
        return jsonify({'result': 'dev'})
    if result == True and isDev == False :
        return jsonify({'result': 'ok'})
    else:
        raise WebException(message = "Login failed", status_code = 401)

@app.route('/user/logout')
def logout():
    UserRepo.Instance().logoutUser()
    return jsonify({'result': 'success'})

@app.route('/user/status')
def status():
    if current_user.is_authenticated:
        a=current_user.roles
        return jsonify({'status': True, 'userName': current_user.name, 'userEmail': current_user.email, 'userRoles': [role.name for role in current_user.roles]})
    return jsonify({'status': False})

@app.route("/coredumps/<coredump_job_id>/get_job_status/", methods = ['GET'])
def getCoredumpJobStatus(coredump_job_id):
    return CoredumpRepo.Instance().getProcessingStatus(coredump_job_id)

@app.route("/coredumps/<coredump_job_id>/get_info/", methods = ['GET'])
def getCoredumpDataInfo(coredump_job_id):
    return CoredumpRepo.Instance().getCoredumpInfo(coredump_job_id)

@app.route('/coredumps/<coredump_job_id>/<variant>/<process_full_path>/<build_version>/<extended_backtrace>/add_job/', methods = ['POST'])
def addCoredumpJob(coredump_job_id, variant , process_full_path, build_version, extended_backtrace):
    return CoredumpsJobProcessorManager.Instance().addJob(coredump_job_id, 'tbd', process_full_path, build_version, extended_backtrace, request.files['file'])


@app.route('/coredumps/<coredump_job_id>/<variant>/<process_full_path>/<build_version>/<extended_backtrace>/add_devjob/', methods = ['POST'])
@roles_required("devuser")
def addDevCoredumpJob(coredump_job_id, variant, process_full_path, build_version, extended_backtrace):
    return CoredumpsJobProcessorManager.Instance().addJob(coredump_job_id,'tbd', process_full_path, build_version, extended_backtrace, request.files['file'])

@app.route('/coredumps/<coredump_job_id>/<variant>/<process_full_path>/<build_version>/<extended_backtrace>/add_job_again/', methods = ['POST'])
def addCoredumpJobAgain(coredump_job_id, variant, process_full_path, build_version, extended_backtrace):
    return CoredumpsJobProcessorManager.Instance().addJobAgain(coredump_job_id, 'tbd', process_full_path, build_version, extended_backtrace)

@app.route('/coredumps/get_all/', methods = ['GET'])
def getAllCoreDumpsIds():
    return CoredumpsJobProcessorManager.Instance().getAllCoreDumpsDoneJobs()



@app.route("/coredumps/<coredump_job_id>/download_file/", methods = ['GET'])
def downloadCoreDumpForCoredumpJob(coredump_job_id):
    archivePath = ""
    try:
        archivePath = CoredumpsJobProcessorManager.Instance().downloadCoreDump(coredump_job_id)
    except Exception as e:
        raise WebException(message="Core dump could not be downloaded", status_code=401)

    return send_file(archivePath, as_attachment=True)


@app.route("/developer_core_dump_job/upload_file/<devfilepath>/<buildVersion>",methods = ['POST'])
@roles_accepted('devuser')
def uploadDeveloperFile(devfilepath,buildVersion):
    devuser=current_user.email
    return CoredumpsJobProcessorManager.Instance().uploadDevFile(request.files['file'],devfilepath,devuser,buildVersion)


@app.route("/developer_core_dump_job/get_status")
@roles_accepted('devuser')
def getStatus():
        return CoredumpsJobProcessorManager.Instance().getDownloadStatus()


@app.route("/developer_core_dump_job/<devname>")
@roles_accepted('devuser')
def getrootfs(devname):
    devname=current_user.email




@app.route('/charts/add', methods = ['POST'])
@roles_accepted('devuser')
def createChart():
    return ChartManager.Instance().createChart(request.json)

if __name__ == "__main__":
    app.run(host = '0.0.0.0', port = 8070, threaded = True)
