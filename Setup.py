import pip
import os
from subprocess import Popen

#####################################################################################################################

# First install Python packages

def install(package):
    pip.main(['install', package])

install('pysmb')
install('pyyaml')
install('argparse')
install('wget')
install('requests')
install('websocket-client')
install('mongoengine')
install('requests')
install('jsonpickle')
install('pymysql')
install('bcrypt')
install('flask')
install('flask_mail')
install('flask_compress')
install('flask_apscheduler')
install('plotly')
install('flask_mongoengine')
install('flask_security')
install('bs4')
#install ('pandas')


#####################################################################################################################

backup_script = os.path.join(os.path.dirname(os.path.realpath(__file__)), "backend", "Scripts", "mongo_backup.bat")

p = Popen(["schtasks", '/create', '/tn', 'MongoBackup', '/tr', backup_script, '/sc', 'daily'])

stdout, stderr = p.communicate()
#####################################################################################################################
