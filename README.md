Internal BMW-RAM team Web application integrates various existing internal tools, such as Backtrace Extractor, Ticket Analyzer, Statistics creator etc.



Getting started:
=================

### ** Prerequisites ** ###

   - Python > 3.0

   - MongoDB

   - Bower (for handling frontend js and css dependencies)

### ** Installing **### 

   - Get MongoDB from https://www.mongodb.com/

   - Get Robomongo from https://robomongo.org/download to view the db

   - Install Bower via nodejs, download it from here https://nodejs.org/en/download/ , after that open a CMD prompt and use 
   
            npm install -g bower

   - Run setup.py to get all the Python dependencies
    
   - Navigate to /frontend
   
   - Open a command prompt (cmd) and use 
   
            bower install
    
   to get all the javascript dependencies


### ** Starting the server: ** ###
    
   - Make sure you have an instance of MongoDB running beforehand -- we recommend installing 
       it as a service
        
   - Run start_server.py

Functional info:
=================

### ** Project Tree ** ### 

        project
        │   README.md
        │   start_server.py
            setup.py
        │
        └───frontend
        │   │   bower.json
        │   │   index.html
        │   │
        │   └───bower_components
        │   │
        │   └───css
        │   │
        │   └───js
        │   │
        │   └───pages
        │
        └───backend
            │
            └───BacktraceExtractor
            │
            └───Config
            │
            └───ElvisTicketDownloader
            │
            └───Misc
            │
            └───MongoDbModels
            │
            └───MongoDbRepos
            │
            └───Plotting
            │
            └───workingDir
        
     
### ** How it works: ** ###


   - It downloads requested ticket attachments from Elvis
        - these are saved in workingDir/ticketDownloads/
    
   - Parses any .DLT files in them and extracts any core dumps using DLTViewer or DobbyLT from workingDir/backtraceExtractorTools
        - after finding what build version is required (from core dump), it downloads 
        that build to workingDir/backtraceExtractorBuilds
            
   - On Windows, it uses "gcc-linaro-arm-linux-gnueabihf-4.8-2013.10_win32" gcc toolchain (found at workingDir/backraceExtractorTools/ to extract the backtrace
        - based on the downloaded builds and the extracted core dumps