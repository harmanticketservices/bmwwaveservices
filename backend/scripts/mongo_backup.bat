@ECHO OFF

cd %~dp0

cd ..

cd workingDir

if not exist "backup_dir" mkdir backup_dir

cd backup_dir

del *.archive /F/Q

mongodump --archive="%date%.archive"
