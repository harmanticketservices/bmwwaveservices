from Globals import *
from Singleton import Singleton
from mongoengine import *
import LoggerFactory

from CoredumpJob import CoredumpJob

@Singleton
class CoredumpJobRepo:
    def __init__(self):
        self.logger = LoggerFactory.getFactoryLogger("CoredumpJobRepo", WORKING_DIR, "server")

    def add(self, coredumpJobId, extendedBacktrace, processFullPath, variant, buildVersion, coredumpPath):
        ob = self.findByCoredumpJobId(coredumpJobId)
        if ob != None:
            self.logger.warning("CoredumpJob {coredumpJobId} already exists exist!".format(coredumpJobId=coredumpJobId))
            return ob.getId()

        if extendedBacktrace == 'false':
            extendedBacktrace = False
        elif extendedBacktrace == 'true':
            extendedBacktrace = True
        ob = CoredumpJob(coredumpJobId=coredumpJobId, isFinished=False, extendedBacktrace=extendedBacktrace,
                       processFullPath=processFullPath, variant=variant, buildVersion=buildVersion, coredumpPath=coredumpPath).save()

        return ob.getId()

    def updateStatus(self, id, status):
        ob = self.findById(id)
        if (ob == None):
            self.logger.warning("CoredumpJob {id} doesn't exist!".format(id=id))
            return

        ob.isFinished = status
        ob.save()

    def eraseAllJobsFinished(self):
        try:
            CoredumpJob.objects(isFinished=True).delete()
        except (ValidationError, CoredumpJob.DoesNotExist) as e:
            pass

    def findAllNotFinished(self):
        list = []
        try:
            list = CoredumpJob.objects(isFinished__ne=True)
        except (ValidationError, CoredumpJob.DoesNotExist) as e:
            pass
        return list

    def findById(self, id):
        ob = None
        try:
            ob = CoredumpJob.objects.get(id=id)
        except (ValidationError, CoredumpJob.DoesNotExist) as e:
            self.logger.warning("CoredumpJob with {id} doesn't exist!".format(id=id))
        return ob

    def findByCoredumpJobId(self, coredumpJobId):
        ob = None
        try:
            ob = CoredumpJob.objects.get(coredumpJobId=coredumpJobId)
        except (ValidationError, CoredumpJob.DoesNotExist) as e:
            pass
            # self.logger.warning("Job with ticketId {ticketId} doesn't exist!".format(ticketId = ticketId))
        return ob

    def deleteById(self, id):
        try:
            CoredumpJob.objects(id=id).delete()
        except (ValidationError, CoredumpJob.DoesNotExist) as e:
            self.logger.warning("CoredumpJob {coredumpJobId} doesn't exist!".format(id=id))

    def deleteByCoredumpJobId(self, coredumpJobId):
        try:
            CoredumpJob.objects(coredumpJobId=coredumpJobId).delete()
        except (ValidationError, CoredumpJob.DoesNotExist) as e:
            self.logger.warning("CoredumpJob {coredumpJobId} doesn't exist!".format(coredumpJobId=coredumpJobId))

    def findAll(self):
        return CoredumpJob.objects