import copy, jsonpickle, json, LoggerFactory, datetime, re
from enum import Enum
from flask import jsonify
from mongoengine import *

from Globals import *
from Singleton import Singleton
from WebException import WebException
from Coredump import Coredump
from CoredumpJobStatusResponse import CoredumpJobStatusResponse

@Singleton
class CoredumpRepo:
    def __init__(self):
        self.logger = LoggerFactory.getFactoryLogger("CoredumpRepo", WORKING_DIR, "server")

    def findByCoredumpId(self, coreDumpId):
        ob = None
        try:
            ob = Coredump.objects.get(coreDumpId=coreDumpId)
        except (ValidationError, Coredump.DoesNotExist) as e:
            pass
            # self.logger.warning("Ticket with {ticketId} doesn't exist!".format(ticketId = ticketId))
        return ob

    def getProcessingStatus(self, coreDumpId):
        ob = self.findByCoredumpId(coreDumpId)
        if ob == None:
            return jsonpickle.encode(CoredumpJobStatusResponse(False, ""), unpicklable=False)
        return jsonpickle.encode(CoredumpJobStatusResponse(ob.isFinished, ""), unpicklable=False)

    def addCoredump(self, coreDumpId, processFullPath, variant, buildVersion, coredumpPath):
        ob = self.findByCoredumpId(coreDumpId)
        if ob != None:
            self.logger.error(message="Coredump {coreDumpId} already exist!".format(coreDumpId=coreDumpId))
            raise WebException(message="Coredump {coreDumpId} already exist!".format(coreDumpId=coreDumpId),
                               status_code=401)
        Coredump(coreDumpId=coreDumpId, processFullPath=processFullPath, buildVersion=buildVersion, variant=variant, coredumpPath=coredumpPath, isFinished=False).save()

    def updateCoredump(self, coreDumpId, processFullPath, variant, buildVersion):
        ob = self.findByCoredumpId(coreDumpId)
        if ob == None:
            self.logger.error(message="Coredump {coreDumpId} does not exist!".format(coreDumpId=coreDumpId))
            raise WebException(message="Coredump {coreDumpId} does not exist!".format(coreDumpId=coreDumpId),
                               status_code=401)
        Coredump.objects(coreDumpId=coreDumpId).update(set__processFullPath=str(processFullPath))
        Coredump.objects(coreDumpId=coreDumpId).update(set__buildVersion=buildVersion)
        Coredump.objects(coreDumpId=coreDumpId).update(set__variant=variant)

    def addSolvedCrash(self, coreDumpId, crash):
        ob = self.findByCoredumpId(coreDumpId)
        if ob == None:
            self.logger.error(message="Coredump {coreDumpId} does not exist!".format(coreDumpId=coreDumpId))
            raise WebException(message="Coredump {coreDumpId} does not exist!".format(coreDumpId=coreDumpId),
                               status_code=401)

        Coredump.objects(coreDumpId=coreDumpId).update(set__crash=crash)
        Coredump.objects(coreDumpId=coreDumpId).update(set__isFinished=True)

    def deleteCrash(self, coreDumpId):
        ob = self.findByCoredumpId(coreDumpId)
        if ob == None:
            self.logger.error(message="Coredump {coreDumpId} does not exist!".format(coreDumpId=coreDumpId))
            raise WebException(message="Coredump {coreDumpId} does not exist!".format(coreDumpId=coreDumpId),
                               status_code=401)

        Coredump.objects(coreDumpId=coreDumpId).update(set__crash=None)
        Coredump.objects(coreDumpId=coreDumpId).update(set__isFinished=False)

    def setBacktraceErrorMessage(self, coreDumpId, errorMessage):
        ob = self.findByCoredumpId(coreDumpId)
        if ob == None:
            self.logger.error(message="Coredump {coreDumpId} does not exist!".format(coreDumpId=coreDumpId))
            raise WebException(message="Coredump {coreDumpId} does not exist!".format(coreDumpId=coreDumpId),
                               status_code=401)
        Coredump.objects(ticketId=coreDumpId).update(set__backtraceErrorMessage=errorMessage)
        Coredump.objects(ticketId=coreDumpId).update(set__isBacktraceSolved=False)

    def getCoredumpInfo(self, coreDumpId):
        ob = self.findByCoredumpId(coreDumpId)
        if ob == None:
            self.logger.error(message="Coredump {coreDumpId} does not exist!".format(coreDumpId=coreDumpId))
            raise WebException(message="Coredump {coreDumpId} does not exist!".format(coreDumpId=coreDumpId),
                               status_code=401)
        return ob.to_json()

    def getAllCoreDumpsData(self):
          return Coredump.objects.only('coreDumpId', 'coredumpPath', 'buildVersion').to_json()


