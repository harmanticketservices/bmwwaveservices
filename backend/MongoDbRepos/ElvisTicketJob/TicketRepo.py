import copy, jsonpickle, json, pymysql.cursors, LoggerFactory, datetime, re
from enum import Enum
from flask import jsonify
from mongoengine import *

from Globals import *
from Singleton import Singleton
from Ticket import Ticket
from Crash import Crash
from CrashRepo import CrashRepo
from TicketElvisInfo import TicketElvisInfo
from WebException import WebException
from TicketJobStatusResponse import TicketJobStatusResponse
from TagRepo import TagRepo
from Tag import Tag

class TicketStatus(Enum):
    dld_attch = 'downloading_attachments'
    proc_attch = 'processing_attachments'
    extr_coredmp = 'extracting_core_dumps'
    dld_build = 'downloading_build'
    create_bt = 'creating_backtraces'
    fin = 'finished'

STATUS_AND_RESPONSE_MAP = {
    TicketStatus.dld_attch.value: "Downloading ticket attachments...",
    TicketStatus.proc_attch.value: "Processing ticket attachments...",
    TicketStatus.extr_coredmp.value: "Extracting coredumps...",
    TicketStatus.dld_build.value: "Downloading required build version...",
    TicketStatus.create_bt.value: "Creating backtraces...",
    TicketStatus.fin.value: "Ticket processing finished."
}

@Singleton
class TicketRepo:
    def __init__(self):
        self.logger = LoggerFactory.getFactoryLogger("TicketRepo", WORKING_DIR, "server")

        self.connectedToElvis = False
        try:
            self.elvisDBConnection = pymysql.connect(host = "elvisreport.harman.com",
                                     user = "SReport",
                                     password = "sm2810jr",
                                     db = "db_output",
                                     charset = "utf8mb4",
                                     cursorclass = pymysql.cursors.DictCursor)
            self.connectedToElvis = True
        except Exception as e:
            self.logger.warning("Could not connect to elvis")

    def refreshElvisConnection(self):
        self.logger.info("Refreshing Elvis SQL connection")

        try:
            self.elvisDBConnection.close()
            self.connectedToElvis = False
            self.elvisDBConnection = pymysql.connect(host="elvisreport.harman.com",
                                                     user="SReport",
                                                     password="sm2810jr",
                                                     db="db_output",
                                                     charset="utf8mb4",
                                                     cursorclass=pymysql.cursors.DictCursor)
            self.connectedToElvis = True
        except Exception as e:
            self.logger.warning("Could not connect to elvis")

    def addSolvedCrashes(self, ticketId, list):

        newCrashIds = []
        newSolvedCrashes = []
        hasKernelPanic = False
        for res in list:
            crashId = res.payload["crash_id"]
            if "KERNEL_PANIC_" in crashId:
                hasKernelPanic = True
            if not crashId in newCrashIds:
                newCrashIds.append(crashId)
            resForDb = copy.deepcopy(res)
            resForDb.payload = json.dumps(resForDb.payload)
            newSolvedCrashes.append(resForDb)

        ob = self.findByTicketId(ticketId)
        if ob == None:
            saveOb = Ticket(ticketId = ticketId, crashIds = newCrashIds, solvedCrashes = newSolvedCrashes, areBacktracesSolved = True, hasKernelPanic = hasKernelPanic).save()
        else:
            for crashId in newCrashIds:
                if not crashId in ob.crashIds:
                    ob.crashIds.append(crashId)
            for newSolvedCrash in newSolvedCrashes:
                newBt = newSolvedCrash.bt
                isNew = True
                for solvedCrash in ob.solvedCrashes:
                    if solvedCrash.bt == newBt:
                        isNew = False
                        break
                if isNew == True:
                   Ticket.objects(ticketId = ticketId).update_one(push__solvedCrashes = newSolvedCrash)
            ob.areBacktracesSolved = True
            ob.hasKernelPanic = hasKernelPanic
            ob.save()

    def updateTicketStatus(self, ticketId, status):
        ob = self.findByTicketId(ticketId)

        if not ob:
            self.logger.warning("Ticket with {ticketId} doesn't exist!".format(ticketId=ticketId))
            return

        ob.ticketStatus = status
        ob.save()

    def changeTags(self, ticketId, newTags):
        ticket = self.findByTicketId(ticketId)
        if ticket == None:
            self.logger.warning("Ticket {ticketId} doesn't exist!".format(ticketId=ticketId))

        for text in newTags:
            ob = TagRepo.Instance().findByText(text)
            if ob == None:
                Tag(text = text).save()

        Ticket.objects(ticketId=ticketId).update(upsert=True, set__tags=newTags)

        return jsonify({'status': True})

    def getAllByTag(self, text):
        ob = TagRepo.Instance().findByText(text)
        if ob == None:
            raise WebException(message="Tag does not exist!", status_code=401)
        return Ticket.objects(tags = text).only('ticketId', 'generalInfo.Title', 'generalInfo.Status').order_by('-ticketId').to_json()

    def addElvisTicketInfo(self, ticketId):

        if not self.connectedToElvis:
            self.logger.warning("Could not connect to elvis db")
            return

        ticket = self.findByTicketId(ticketId)
        if ticket != None and ticket.generalInfo.Status == "Closed":
           return

        sqlQuery =  "SELECT TicketID AS Ticket, RTRIM(Title) AS Title, Sys_SWRev AS SoftwareVersion, PriorityID AS Priority, Category AS Category, FGroup AS Domain, " \
					"ResponsibleUser AS Responsible, FixPlannedInCW AS Planned, TicketStepID AS Status, EnterDateTime AS Reported, " \
					"LastRespDateTime AS Categorizing, LastProcDateTime AS Processing, LastConclDateTime AS Concluding, ProcessingUser AS Developer," \
					"CASE YEAR(LastConclDateTime) " \
					"WHEN 2015 THEN WEEKOFYEAR(LastConclDateTime)+1500 " \
					"WHEN 2016 THEN WEEKOFYEAR(LastConclDateTime)+1600 " \
					"WHEN 2017 THEN WEEKOFYEAR(LastConclDateTime)+1700 " \
					"WHEN 2018 THEN WEEKOFYEAR(LastConclDateTime)+1800 ELSE 0	END AS Closed, " \
					"IF(LastConclDateTime<>'0000-00-00', DATEDIFF(LastConclDateTime, EnterDateTime), DATEDIFF(CURDATE(), EnterDateTime)) AS Age, " \
					" CASE TicketStepID " \
					"WHEN 'Categorizing' THEN 0 " \
					"WHEN 'Reproduction' THEN 1 " \
					"WHEN 'Processing' THEN 2 " \
					"WHEN 'PreIntegrating' THEN 3 " \
					"WHEN 'Integrating' THEN 4 " \
					"WHEN 'Verifying' THEN 5 " \
					"WHEN 'Concluding' THEN 6 " \
					"WHEN 'Closed' THEN 7 END AS StatusInt " \
					"FROM tbl_ElvisSR " \
					"WHERE (TicketID = {ticketId})".format(ticketId = ticketId)

        result = None
        with self.elvisDBConnection.cursor() as cursor:
            rows_count = 0
            try:
                rows_count = cursor.execute(sqlQuery)
            except Exception as e:
                self.logger.info(str(e))
            if rows_count > 0:
                result = cursor.fetchall()
     
        if result == None:
            self.logger.warning("Ticket with {ticketId} doesn't exist in Elvis!".format(ticketId = ticketId))
            return

        #Added a datetime object to be passed to Processing Field in the SQL Result because tickets with 0000-00-00 processing date crash the JSON processing part
        auxProcessingTime= datetime.datetime.today().strftime('%Y-%m-%d')
        info = TicketElvisInfo(
            Ticket = result[0]["Ticket"],
            Title = result[0]["Title"],
            SoftwareVersion = result[0]["SoftwareVersion"],
            Priority = result[0]["Priority"],
            Category = result[0]["Category"],
            Domain = result[0]["Domain"],
            Responsible = result[0]["Responsible"],
            Planned = result[0]["Planned"],
            Status = result[0]["Status"],
            Reported = result[0]["Reported"],
            Categorizing = result[0]["Categorizing"],
            Processing = auxProcessingTime,
            Age = result[0]["Age"],
            Developer = result[0]["Developer"])

        ticket = self.findByTicketId(ticketId)
        if ticket == None:
            saveOb = Ticket(ticketId = ticketId, generalInfo = info).save()
        else:
            ticket.generalInfo = info
            ticket.save()

    def getDuplicateTicketsFor(self, ticketId):
        ticket = self.getTicketInfo(ticketId)
            
        res = {}
        res['dupsByCrash'] = {}
        res['all'] = set()
                               
        for crashId in ticket.crashIds:
            crashOb = CrashRepo.Instance().findByCrashId(crashId)
            if crashOb == None:
                continue
            res['dupsByCrash'][crashId] = []
            for ticket in crashOb.tickets:
                if ticket != ticketId:
                    res['dupsByCrash'][crashId].append(ticket)
                    res['all'].add(ticket)
        
        return jsonpickle.encode(res, unpicklable = False)        

    def deleteCrashesFromDb(self, ticketId):
        ticket = self.findByTicketId(ticketId)
        if ticket == None:
            raise WebException(message="Ticket {ticketId} doesn't exist!".format(ticketId = ticketId),
                               status_code=401)

        # first erase from crash id table
        for crashId in ticket.crashIds:
            ob = CrashRepo.Instance().findByCrashId(crashId)
            if ob == None:
                continue
            Crash.objects(crashId=crashId).update(pull__tickets=ticketId)

        Ticket.objects(ticketId=ticketId).update(set__backtracesErrorMessage = "")
        Ticket.objects(ticketId=ticketId).update(set__areBacktracesSolved = False)
        Ticket.objects(ticketId=ticketId).update(set__solvedCrashes = [])
        Ticket.objects(ticketId=ticketId).update(set__crashIds = [])
        Ticket.objects(ticketId=ticketId).update(set__ticketStatus = "Pending to be processed..")

    def setBacktraceErrorMessage(self, ticketId, errorMessage):
        ticket = self.findByTicketId(ticketId)
        if ticket == None:
            self.logger.warning("Ticket {ticketId} doesn't exist!".format(ticketId=ticketId))
        Ticket.objects(ticketId=ticketId).update(set__backtracesErrorMessage=errorMessage)
        Ticket.objects(ticketId=ticketId).update(set__areBacktracesSolved=False)

    def getTicketInfo(self, ticketId):
        self.addElvisTicketInfo(ticketId)

        ticket = self.findByTicketId(ticketId)

        if ticket != None:
            return ticket

        raise WebException(message = "Ticket with id {ticketId} doesn't exist in Elvis!".format(ticketId = ticketId), status_code = 401)
    
    def getAllTicketsIds(self):
          return Ticket.objects.only('ticketId', 'generalInfo.Title', 'generalInfo.Status').order_by('-ticketId')

    def getProcessingStatus(self, ticketId):
        ob = self.findByTicketId(ticketId)
        if ob == None:
            raise WebException(message="Ticket with id {ticketId} doesn't exist in db!".format(ticketId=ticketId),
                               status_code=401)
        return jsonpickle.encode(TicketJobStatusResponse(ob.ticketStatus, ob.backtracesErrorMessage), unpicklable=False)


    def findByTicketId(self, ticketId):
        ob = None
        try:
            ob = Ticket.objects.get(ticketId = ticketId)
        except (ValidationError, Ticket.DoesNotExist) as e:
            pass
            # self.logger.warning("Ticket with {ticketId} doesn't exist!".format(ticketId = ticketId))
        return ob

    def findTicketsFromRawQuery(self, rawQuery):
        obs = []
        rawQuery = self.fixDateFormatInQuery(rawQuery)

        try:
            obs = Ticket.objects(__raw__ = rawQuery)
        except (ValidationError, Ticket.DoesNotExist) as e:
            pass
        return obs.to_json()

    def fixDateFormatInQuery(self, queryDict):
        p = re.compile(r'(\d\d\d\d)\/(\d\d)\/(\d\d)')

        try:
            if isinstance(queryDict, dict):
                for value in queryDict.values():
                    if isinstance(value, list):
                        for item in value:
                            self.fixDateFormatInQuery(item)
                    else:
                            for key, val in value.items():
                                s = p.search(val)
                                if s:
                                    nYear = int(s.group(1))
                                    nMonth = int(s.group(2))
                                    nDay = int(s.group((3)))
                                    value[key] = datetime.datetime(nYear, nMonth, nDay)
        except Exception:
            pass  # fail silently

        return queryDict

    def addDummyDuplicateTickets(self):
        Ticket(ticketId="1111111", crashIds=["1", "2", "3"], solvedCrashes=[], areBacktracesSolved=False).save()
        Ticket(ticketId="1111112", crashIds=["2", "1"], solvedCrashes=[], areBacktracesSolved=False).save()
        Ticket(ticketId="1111113", crashIds=["3"], solvedCrashes=[], areBacktracesSolved=False).save()
        Crash(crashId="1", tickets=["1111111", "1111112"]).save()
        Crash(crashId="2", tickets=["1111111"]).save()
        Crash(crashId="3", tickets=["1111111", "1111113"]).save()


