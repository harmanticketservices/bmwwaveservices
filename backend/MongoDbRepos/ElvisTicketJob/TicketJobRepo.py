from Globals import *
from Singleton import Singleton
from mongoengine import *
import LoggerFactory

from TicketJob import TicketJob

@Singleton
class TicketJobRepo:
    def __init__(self):
        self.logger = LoggerFactory.getFactoryLogger("TicketJobRepo", WORKING_DIR, "server")

    def add(self, ticketId, extendedBacktrace, processFullPath, variant):
        ob = self.findByTicketId(ticketId)
        if ob != None:
            self.logger.warning("Job for ticket it {ticketId} already exists exist!".format(ticketId=ticketId))
            return ob.getId()

        ob = TicketJob(ticketId = ticketId, isFinished = False, extendedBacktrace = extendedBacktrace, processFullPath = processFullPath, variant = variant).save()
        
        return ob.getId()

    def addDevJob(self,ticketId,extendedBacktrace,processFullPath,AplicationFullPath,variant):
        ob =  self.findByTicketId(ticketId)
        if ob != None:
            self.logger.warning("Job for ticket id {tickedId} already exists!".format(ticketId=ticketId))
            return ob.getId()

        ob=TicketJob(ticketId=ticketId, isFinished=False,extendedBacktrace = extendedBacktrace, processFullPath =processFullPath, AplicationFullPath = AplicationFullPath, variant=variant).save()

        return ob.getId()


    def updateStatus(self, id, status):
        ob = self.findById(id)
        if (ob == None):
            self.logger.warning("Job with {id} doesn't exist!".format(id = id))
            return

        ob.isFinished = status
        ob.save()

    def eraseAllJobsFinished(self):
        try:
            TicketJob.objects(isFinished=True).delete()
        except (ValidationError, TicketJob.DoesNotExist) as e:
            pass

    def findAllNotFinished(self):
        list = []
        try:
            list = TicketJob.objects(isFinished__ne = True)
        except (ValidationError, TicketJob.DoesNotExist) as e:
            pass
        return list
            
    def findById(self, id):
        ob = None
        try:
            ob = TicketJob.objects.get(id = id)
        except (ValidationError, TicketJob.DoesNotExist) as e:
            self.logger.warning("Job with {id} doesn't exist!".format(id = id))
        return ob

    def findByTicketId(self, ticketId):
        ob = None
        try:
            ob = TicketJob.objects.get(ticketId = ticketId)
        except (ValidationError, TicketJob.DoesNotExist) as e:
            pass
            # self.logger.warning("Job with ticketId {ticketId} doesn't exist!".format(ticketId = ticketId))
        return ob

    def deleteById(self, id):
        try:
            TicketJob.objects(id = id).delete()
        except (ValidationError, TicketJob.DoesNotExist) as e:
            self.logger.warning("Job with ticketId {ticketId} doesn't exist!".format(ticketId = ticketId))

    def deleteByTicketId(self, ticketId):
        try:
            TicketJob.objects(ticketId = ticketId).delete()
        except (ValidationError, TicketJob.DoesNotExist) as e:
            self.logger.warning("Job with ticketId {ticketId} doesn't exist!".format(ticketId = ticketId))       

    def findAll(self):
       return TicketJob.objects
