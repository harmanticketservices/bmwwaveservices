from Globals import *
from Singleton import Singleton
from mongoengine import *
from Tag import Tag
import LoggerFactory

@Singleton
class TagRepo:
    def __init__(self):
        self.logger = LoggerFactory.getFactoryLogger("TagRepo", WORKING_DIR, "server")

    def findByText(self, text):
        ob = None
        try:
            ob = Tag.objects.get(text=text)
        except Exception as e:
            pass
        return ob

    def getAllTags(self):
        return Tag.objects.only('text')