import LoggerFactory

from mongoengine import *
from Globals import *
from Singleton import Singleton
from Crash import Crash


@Singleton
class CrashRepo:
    def __init__(self):
        self.logger = LoggerFactory.getFactoryLogger("CrashRepo", WORKING_DIR, "server")
    
    def addCrashes(self, ticketId, list):
        for res in list:
            crashId = res.payload["crash_id"]
            ob = self.findByCrashId(crashId)
            if ob == None:
              saveOb = Crash(crashId = crashId, tickets = [ticketId]).save()
            else:
                if not ticketId in ob.tickets:
                    ob.tickets.append(ticketId)
                    ob.save()

    def findByCrashId(self, crashId):
        ob = None
        try:
            ob = Crash.objects.get(crashId = crashId)
        except (ValidationError, Crash.DoesNotExist) as e:
            # self.logger.warning("Crash with {crashId} doesn't exist!".format(crashId = crashId))
            pass
        return ob

    def getAllCrahesIds(self):
        return Crash.objects.only('crashId', 'tickets')



