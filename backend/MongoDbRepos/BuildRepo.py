import os.path

import LoggerFactory, MiscUtils

from mongoengine import *
from Globals import *
from Singleton import Singleton
from Build import Build

@Singleton
class BuildRepo:
    def __init__(self):
        self.logger = LoggerFactory.getFactoryLogger("CrashRepo", WORKING_DIR, "server")

    def addBuild(self, version, variantNumber, relativePath, isEmergency):
        ob = self.findBuild(version, variantNumber)
        if ob is None:
            self.logger.info("Adding build with version {version}, variant {variantNumber} to db.".format(version=version, variantNumber=variantNumber))
            saveOb = Build(version=version, variantNumber=variantNumber, relativePath=relativePath, isEmergency=isEmergency).save()

    def findBuild(self, version, variantNumber):
        ob = None

        try:
            ob = Build.objects.get(version=version, variantNumber=variantNumber)
        except (ValidationError, Build.DoesNotExist) as e:
            self.logger.warning("Build with version {version}, variant {variantNumber} doesn't exist!".format(version=version, variantNumber=variantNumber))
            pass
        return ob

    def getBuildLocalPath(self, version, variantNumber):
        ob = self.findBuild(version, variantNumber)

        if ob:
            return ob.relativePath
        return ob

    def getBuildAbsolutePath(self, version, variantNumber):
        ob = self.findBuild(version, variantNumber)

        if ob:
            return os.path.join(BACKTRACE_EXTRACTOR_BUILDS, 'targets', ob.relativePath)
        return ob

    def add_local_builds_to_db(self):

        builds_dir = os.path.join(BACKTRACE_EXTRACTOR_BUILDS, 'targets')

        build_folders = MiscUtils.get_fs_tree_from_path(builds_dir)
        possible_builds = [s for s in build_folders if '.tar.bz2' in s]

        real_builds = []
        for build in possible_builds:
            real_builds.append(build.split('.tar.bz2')[0].split('targets')[1][1:])

        for build in real_builds:
            tmpB = build.split('\\')

            version = tmpB[2].split('.')[0]
            variant = tmpB[4].split('-')[1][1:]

            isEmergency = True if 'emergency' in build else False

            self.addBuild(version, variant, build, isEmergency)
