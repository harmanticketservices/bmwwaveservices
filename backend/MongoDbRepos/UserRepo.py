from flask_security import login_user, logout_user
from flask_security.utils import verify_password, hash_password

import LoggerFactory
from Globals import *
from Singleton import Singleton
from WebException import WebException
from User import User
from flask import jsonify

@Singleton
class UserRepo:
    def __init__(self):
        self.logger = LoggerFactory.getFactoryLogger("UserRepo", WORKING_DIR, "server")
        self.user_datastore = None

    def setUserDatastore(self, user_datastore):
        self.user_datastore = user_datastore

    def addUser(self, name, email, password):
        ob = self.findByEmail(email)
        if ob == None:
            self.user_datastore.create_user(name=name, email=email, password=hash_password(password))
            userRole = self.user_datastore.find_or_create_role(name="user", description="Regular user")
            self.user_datastore.add_role_to_user(email, userRole)
            return jsonify({'result': 'ok'})
        else:
            raise WebException(message="User already exists", status_code=401)

    def checkDeveloper(self,email):
        object=self.findByEmail(email)
        result = object.has_role("devuser")
        if result == True:
            return True
        else:
            return False

    def addDeveloper(self,email):
        ob=self.findByEmail(email)
        if ob != None:
            developerRole=self.user_datastore.find_or_create_role(name="devuser", description= "Developer")
            self.user_datastore.add_role_to_user(email, developerRole)
        else :
            return True




    def authenticateUser(self, email, password):
        ob = self.findByEmail(email)

        if ob == None:
            raise WebException(message="No user found", status_code=404)
            return False

        if not verify_password(password, ob.password):
            raise WebException(message="Wrong Password", status_code=401)
            return False

        if not login_user(ob):
            raise WebException(message="Couldn't login", status_code=401)
            return False

        return True

    def logoutUser(self):
        logout_user()

    def promoteToAdmin(self, email):
        adminRole = self.user_datastore.find_or_create_role(name="admin", description="Administrator")
        self.user_datastore.add_role_to_user(email, adminRole)




    def findByEmail(self, email):
        ob = None
        try:
            ob = self.user_datastore.get_user(email)
        except (ValidationError, User.DoesNotExist) as e:
            pass
        return ob




