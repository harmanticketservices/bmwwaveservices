import sys

from flask_security import Security, MongoEngineUserDatastore
from flask_mongoengine import MongoEngine
from flask_apscheduler import APScheduler
from flask_compress import Compress

from TicketDownloaderConfig import *
from Globals import *
from Singleton import Singleton
from ElvisTicketProcessor import ElvisTicketProcessor
import LoggerFactory
from TicketRepo import TicketRepo
from UserRepo import UserRepo
from BuildRepo import BuildRepo
from User import User
from Role import Role

refreshElvisConnectionJob = TicketRepo.Instance().refreshElvisConnection

class Config(object):
    JOBS = [
        {
            'id': 'refreshElvisConn',
            'func': 'AppInitializer:refreshElvisConnectionJob',
            'args': (),
            'trigger': 'interval',
            'minutes': 30
        }
    ]
    SECRET_KEY = 'super secret key'
    MONGODB_DB = DATABASE_NAME
    MONGODB_HOST = DB_HOST
    MONGODB_PORT = 27017
    SECURITY_PASSWORD_HASH = 'bcrypt'
    SECURITY_PASSWORD_SALT = 'QxLUF1bg'

@Singleton
class AppInitializer:

    def __init__(self):
        pass


    def initConfig(self, app):

        app.config.from_object(Config())

    def InitApp(self, app):

        sys.setrecursionlimit(10000)

        if not os.path.isdir(WORKING_DIR):
            os.makedirs(WORKING_DIR)

        self.logger = LoggerFactory.getFactoryLogger("Globals", WORKING_DIR, "server")

        if not os.path.isdir(download_root):
            os.makedirs(download_root)
            self.logger.info("created {dir}".format(dir = download_root))

        if not os.path.isdir(BACKTRACE_EXTRACTOR_TOOLS):
            os.makedirs(BACKTRACE_EXTRACTOR_TOOLS)
            self.logger.info("created {dir}".format(dir = BACKTRACE_EXTRACTOR_TOOLS))

        if not os.path.isdir(BACKTRACE_EXTRACTOR_BUILDS):
            os.makedirs(BACKTRACE_EXTRACTOR_BUILDS)
            self.logger.info("created {dir}".format(dir = BACKTRACE_EXTRACTOR_BUILDS))

        ElvisTicketProcessor.Instance().checkAllToolsArePresent()

        self.initConfig(app)

        # compress
        Compress(app)

        mongoDb = MongoEngine()
        mongoDb.init_app(app)

        scheduler = APScheduler()
        scheduler.api_enabled = True
        scheduler.init_app(app)
        scheduler.start()

        user_datastore = MongoEngineUserDatastore(mongoDb, User, Role)
        UserRepo.Instance().setUserDatastore(user_datastore)
        app.Security = Security(app, user_datastore)

        # BuildRepo.Instance().add_local_builds_to_db()



