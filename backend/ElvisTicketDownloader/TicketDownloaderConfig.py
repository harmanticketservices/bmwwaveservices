import os

url = 'https://elvis.harman.com/cgi-bin/login'
files_to_exclude = []
download_root = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), "workingDir", "ticketDownloads")
parallel_download = True
