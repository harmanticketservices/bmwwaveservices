import shutil
import requests
from bs4 import BeautifulSoup
from urllib.parse import quote
import os
import concurrent.futures

from TicketDownloaderConfig import *
from Globals import *
from Singleton import Singleton
import LoggerFactory

@Singleton
class TicketDownloader:

    def __init__(self):
       self.logger = LoggerFactory.getFactoryLogger("TicketDownloader", WORKING_DIR, "server")
       self.taskInProgress = False

    def get_ticket(self):
        login_data = dict(AID=10003, TID=int(self.ticketId), L=1, nocache=1478515358884)
        self.httpResponseData = self.requestSession.post('https://elvis.harman.com/cgi-bin/ticket_rpc?', data=login_data)

    def url_for(self, soup_split):
        for line in soup_split:
            pos1 = line.find('"id":')
            # reset the line to 0 so that find returns the first occurence after first search
            line = line[pos1:]
            pos1 = line.find('"id":')
            pos2 = line.find('","')

            if pos1 >= 0 and pos2 > 0:
                name = line[pos1 + 5:pos2]
                pos3 = name.find(',"d":["')
                AID = name[0:pos3]
                name = name[pos3 + 7:]
                url = 'https://elvis.harman.com/php/ticket.attachment.link.outside.php?TID=' + self.ticketId + '&FILE=' + \
                      quote(name, safe='') + '&AID=' + AID

                if not any(excluded_file.lower() in name.lower() for excluded_file in files_to_exclude):
                    self.listOfFiles.add((self.requestSession, url, name))
                else:
                    self.logger.info("File {file} excluded from download".format(file = name))

    def find_attach(self):
        soup = BeautifulSoup(self.httpResponseData.content, 'html.parser')
        soup = str(soup)
        soup_split = soup.split(sep='},{')
        self.url_for(soup_split)

    def login(self):
        self.requestSession.get(url)
        login_data = dict(LOGIN=USERNAME, PWD=PASSWORD, SYSTEM='ELVIS', PAGE=1, LANG=0)
        self.httpResponseData = self.requestSession.post(url, data=login_data)

    def download_files(self):
        self.logger.info("Found {} files to download on ticket #{}".format(len(self.listOfFiles), self.ticketId))

        for index, elem in enumerate(self.listOfFiles):
            c = elem[0]
            r = c.get(elem[1], stream=True)

            download_dir_absolute_path = os.path.join(download_root, self.ticketId, self.folderToDownload)
            if not os.path.isdir(download_dir_absolute_path):
                os.makedirs(download_dir_absolute_path)
                self.logger.info("Made directory for " + self.ticketId)

            file_path = os.path.join(download_root, self.ticketId, self.folderToDownload, elem[2])
            with open(file_path, 'wb') as f:
                self.logger.info("{} of {} --> DOWNLOADING: {}".format(index + 1, len(self.listOfFiles), file_path))
                shutil.copyfileobj(r.raw, f)

        self.taskInProgress = False
        return len(self.listOfFiles)

    def loginAndDownload(self, c, tid, folderToDownload):
        
        if self.taskInProgress == True:
            raise Exception("Another download is in progress")

        self.taskInProgress = True
        self.folderToDownload = folderToDownload
        self.requestSession = c
        self.listOfFiles = set()
        self.ticketId = str(tid)
        self.httpResponseData = ""

        self.login()
        self.get_ticket()
        self.find_attach()

        return self.download_files()

        

# x = TicketDownloader.Instance().loginAndDownload(requests.Session(), 2065925)