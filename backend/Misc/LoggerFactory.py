import logging
import os
import sys

import logging
from websocket import create_connection

class loggingWebsocketHandler(logging.Handler):

    def __init__(self, host, port):
        logging.Handler.__init__(self)
        self.host = host
        self.port = port

        self.connected_to_web_socket = False
        try:
            self.web_socket = create_connection("ws://" + host + ":" + str(port))
            self.connected_to_web_socket = True
        except Exception as e:
            print (e)

    def flush(self):
        """
        does nothing for this handler
        """

    def emit(self, record):
        """
        Emit a record.

        """
        try:
            msg = self.format(record)
            if self.connected_to_web_socket:
                self.web_socket.send(msg)
        except Exception as e:
            print (e)
            self.handleError(record)
    
def getFactoryLogger(logger_name, logger_dir, logger_filename):

    logFormatter = logging.Formatter("[%(threadName)-9s] [%(asctime)s] [(%(filename)s:%(lineno)s)] [%(levelname)s] %(message)s")
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.INFO)

    fileHandler = logging.FileHandler("{0}/{1}.log".format(logger_dir, logger_filename))
    fileHandler.setFormatter(logFormatter)
    logger.addHandler(fileHandler)

    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    logger.addHandler(consoleHandler)

    # websocketHandler = loggingWebsocketHandler("localhost", "8000")
    # websocketHandler.setFormatter(logFormatter)
    # logger.addHandler(websocketHandler)

    return logger