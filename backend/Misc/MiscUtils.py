import os.path
import subprocess
import logging
import requests
import ctypes

def download_file(url, to_path):
    r = requests.get(url, stream=True)
    with open(to_path, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024): 
            if chunk: # filter out keep-alive new chunks
                f.write(chunk)
                #f.flush() commented by recommendation from J.F.Sebastian

def create_dir_if_not_exists(path):
    if not os.path.isdir(path):
        os.makedirs(path)

def remove_dir(folder):
    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        if os.path.isfile(file_path):
            os.unlink(file_path)

# Helper method for running a command in OS shell directly
def exec_os_command(command):
    command = command.replace("\\", "/")
    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    stdout, stderr = process.communicate(timeout=300)
    return process.returncode

# Helper method to check if the current operating system is Windows or Posix
def is_os_windows():
    if os.name == 'nt':
        return True
    return False

# Helper method to detect if an app is installed on both Windows/Linux (will return False on Windows if it's not included in PATH)
def is_app_installed(app_name):
    if is_os_windows():
        stdout, stderr = subprocess.Popen(["where", app_name], stdout=subprocess.PIPE).communicate()
        stdout = stdout.decode("utf-8").replace('\n', '').replace('\r', '')
        return os.path.isfile(stdout)
    else:
        stdout, stderr = subprocess.Popen(["which", app_name], stdout=subprocess.PIPE).communicate()
        return not not stdout

# Helper method to get the path where a certain app is installed
def where_is_app_installed(app_name):
    if not is_app_installed(app_name):
        return None

    if is_os_windows():
        stdout, stderr = subprocess.Popen(["where", app_name], stdout=subprocess.PIPE).communicate()
        stdout = stdout.decode("utf-8").replace('\n', '').replace('\r', '')
    else:
        stdout, stderr = subprocess.Popen(["which", app_name], stdout=subprocess.PIPE).communicate()

    return os.path.dirname(stdout)

# Helper method for converting a regular windows path in a cygwin-like one
def convert_in_cygwin_path(path):
    cygwin_path = r"/cygdrive/" + path.split(":")[0] + path.split(":")[1]
    return cygwin_path

def is_cigwin_present():
    for cygwin_bin in [r'C:\cygwin\bin', r'C:\cygwin64\bin']:
        if os.path.isdir(cygwin_bin):
            return True
    return False

# Helper method which returns True if path is an archive which
def is_archive(path):
    archive_extensions = ["7z", "xz", "wim", "tar", "gzip", "bzip2", "gz", "zip"]
    path_ext = path.split(".")[-1]
    if path_ext in archive_extensions and not os.path.isdir(path):
        return True
    return False

def is_splitted_archive(path):
    no_part = path.split(".")[-1]
    try:
        path_ext = path.split(".")[-2]
    except Exception as e:
        return False
    archive_extensions = ["7z", "xz", "wim", "tar", "gzip", "bzip2", "gz", "zip"]
    if no_part == "001" and path_ext in archive_extensions:
        return True
    return False 

# Helper method which returns all files with their from a root directory
def get_fs_tree_from_path(for_path):
    fs_tree = []
    for root, dirs, files in os.walk(for_path):
        for name in files:
            # logger.info(os.path.join(root, name))
            fs_tree.append(os.path.join(root, name))
    return fs_tree

# Helper method for running a Bash command with Cygwin and return output.
def exec_cygwin_command(command):
    # Find Cygwin binary directory
    for cygwin_bin in [r'C:\cygwin\bin', r'C:\cygwin64\bin']:
        if os.path.isdir(cygwin_bin):
            break
    else:
        raise RuntimeError('Cygwin not found!')
    # Make sure Cygwin binary directory in path
    if cygwin_bin not in os.environ['PATH']:
        os.environ['PATH'] += ';' + cygwin_bin
    # Launch Bash
    p = subprocess.Popen(
        args=['bash', '-c', command],
        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    # Raise exception if return code indicates error
    if p.returncode != 0:
        raise RuntimeError(p.stderr.read().rstrip())
    return p.returncode

def check_if_have_admin_rights():
    try:
        is_admin = os.getuid() == 0
    except AttributeError:
        is_admin = ctypes.windll.shell32.IsUserAnAdmin() != 0
    return is_admin
