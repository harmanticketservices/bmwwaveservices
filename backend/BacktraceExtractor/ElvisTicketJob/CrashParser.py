import json, os, logging, tempfile, hashlib, shutil
from smb.SMBConnection import SMBConnection

from Globals import *
from TicketJobResultResponse import TicketJobResultResponse
from BuildRepo import BuildRepo
from TicketRepo import TicketRepo, TicketStatus

import MiscUtils, LoggerFactory

########################################################################################################################

logger = None

# DLT-specific methods
########################################################################################################################

# Method parses a DLT log line read from a file and returns a JSON object 
def parse_line(line, process_full_path, variant):
    buffer = line.split(" ")
    data = {}
    data['Time'] = buffer[1] + " " + (buffer[2].split("."))[0]
    data['Timestamp'] = buffer[3]
    data['Count'] = buffer[4]
    data['Ecuid'] = buffer[5]
    data['Apid'] = buffer[6]
    data['Ctid'] = buffer[7]
    data['SessionId'] = buffer[8]
    data['Type'] = buffer[9]
    data['Subtype'] = buffer[10]
    data['Mode'] = buffer[11]
    data['#Args'] = buffer[12]
    payload = ''.join(buffer[13:len(buffer)])
    try:
        data['Payload'] = json.loads(payload) 
    except Exception as e:
        return ("", False)
    if data['Payload']['exec'].find('MCORE') != -1:
        return ("", False)
    if data['Payload']['exec'].find('DSP') != -1:
        return ("", False)
    if process_full_path != "":
        logger.info("Set for \"full path\" {process_full_path} as set by the user!".format(process_full_path = process_full_path))
        data['Payload']['full_path'] = process_full_path
    if variant != "":
        logger.info("Set \"variant\" {variant} as set by user!".format(variant = variant))
        data['Payload']['variant'] = variant
    elif not 'variant' in data['Payload']:
        data['Payload']['variant'] = ""
    return (data, True)

# Method parses a DLT .txt log file line by line 
# Returns a JSON array
def parse_logfile(filepath, process_full_path, variant):
    data = []
    try:        
        with open(filepath) as f:
            for line in f:
                data_parsed, was_correctly_parsed = parse_line(line, process_full_path, variant)
                if was_correctly_parsed == True:
                    data.append(data_parsed)
                else:
                    logger.warning("Invalid payload: " + line + ". ignoring it..")
    except Exception as e:
        logger.error(str(e))
    return data

# Method parses multiple DLT .txt log files and loads into memory
# the data for further processing
def parse_logfiles(logs_dir, process_full_path, variant):
    all_files = MiscUtils.get_fs_tree_from_path(logs_dir)
    data = []
    if not all_files:
        logger.warning("No logfiles found!")
        return data
    for path in all_files:
        logger.info("Parsing " + path)
        data.extend(parse_logfile(path, process_full_path, variant))
    return data

# Method converts .dlt file to a .txt file with a specific DLT filter applied
def create_logfile_from_dlt(dlt_viewer_bin, dlt_file, dlt_filter, output_log):
    command = "{dlt_viewer_bin} -c \"{dlt_file}\" \"{out_log}\" -f \"{dlt_filter}\"".format(dlt_viewer_bin=dlt_viewer_bin, dlt_file=dlt_file, out_log=output_log, dlt_filter=dlt_filter)

    return MiscUtils.exec_os_command(command)

# Method creates core dumps from a .dlt file
def create_core_dumps_drom_dlt(dlt_viewer_bin, dlt_file, output_dir):
    command = "{dlt_viewer_bin} -s -l \"{dlt_file}\" -e \"Filetransfer Plugin|export|{out_dir}\"".format(dlt_viewer_bin=dlt_viewer_bin, dlt_file=dlt_file, out_dir=output_dir)

    return MiscUtils.exec_os_command(command)

def create_core_dumps_drom_dlt_using_dobbylt(dobbylt_bin, dlt_file, output_dir):
    command = "{dobbylt_bin} -d {dlt_file} -o {out_dir}".format(dobbylt_bin=dobbylt_bin, dlt_file=dlt_file, out_dir=output_dir)

    return MiscUtils.exec_os_command(command)

# Method extracts the core dumps from archives
def extract_core_dumps(core_dumps_dir):
    logger.info("Extracting core dumps...")

    initial_files = MiscUtils.get_fs_tree_from_path(core_dumps_dir)
    for path in initial_files:
        if MiscUtils.is_archive(path):
            command = "7z x \"{in_path}\" -o\"{core_dumps_dir}\" -y > nul".format(in_path=path, core_dumps_dir=core_dumps_dir)
            try:
                MiscUtils.exec_os_command(command)
                logger.info("Extracted " + path)
            except Exception as e:
                logger.error("Error when extracting " + path + " Exeption thrown: " + str(e))
        elif "kernel-panic.tgz" in path:
            logger.info("Found kernel panic core dump !")
            kernel_panic_dir = os.path.join(core_dumps_dir, "kernel-panic")
            MiscUtils.create_dir_if_not_exists(kernel_panic_dir)
            try:
                if MiscUtils.is_os_windows():
                    # since using tar from cygwin shell, properly convert paths to "/cigdrive/E/path/to/file"
                    command = "tar -xf {archive_location} -C {out_path}".format(archive_location=MiscUtils.convert_in_cygwin_path(path), out_path=MiscUtils.convert_in_cygwin_path(kernel_panic_dir))
                    command = command.replace("\\", "/")
                    MiscUtils.exec_cygwin_command(command)
                else:
                    command = "tar -xf {archive_location} -C {out_path}".format(archive_location=path, out_path=kernel_panic_dir)
                    MiscUtils.exec_os_command(command)
            except Exception as e:
                logger.info("Error when extracting kernel panic! Exception thrown: {ex}".format(ex = str(e)))

    initial_and_extracted_files = MiscUtils.get_fs_tree_from_path(core_dumps_dir)
    for path in initial_and_extracted_files:
        if MiscUtils.is_archive(path):
            os.remove(path)
            logger.info("Removed " + path)

def extract_from_archives(dlt_dir):
    logger.info("Searching for archived files in " + dlt_dir)
    initial_files = MiscUtils.get_fs_tree_from_path(dlt_dir)
    for path in initial_files:
        if MiscUtils.is_archive(path):
            command = "7z x \"{in_path}\" -o\"{out_path}\" -y > nul".format(in_path=path, out_path=os.path.splitext(path)[0])
            try:
                MiscUtils.exec_os_command(command)
                logger.info("Extracted " + path + " to " + os.path.splitext(path)[0])
                os.remove(path)
                logger.info("Removed " + path)
            except Exception as e:
                logger.error("Error when extracting " + path + " Exeption thrown: " + str(e))

# Method uses DobbyLT to produce core dumps and logfiles from .dlt files
def create_logfiles_and_core_dumps_from_dlts_using_dobbylt(dlt_dir, output_dir, dobbylt_bin):
    # first, make sure there are no spaces in input file names
    initial_files = MiscUtils.get_fs_tree_from_path(dlt_dir)
    for path in initial_files:
        filename = os.path.basename(path)
        dirname = os.path.dirname(path)
        if ' ' in filename:
            filename = filename.replace(' ', '_')
            try:
                os.rename(path, os.path.join(dirname, filename))
            except Exception as e:
                logger.error(str(e))

    logger.info("Searching for archived files in " + dlt_dir)
    extract_from_archives(dlt_dir)
    initial_files = MiscUtils.get_fs_tree_from_path(dlt_dir)

    for path in initial_files:
        if MiscUtils.is_splitted_archive(path):
            command = "7z x \"{in_path}\" -o\"{out_path}\" -y > nul".format(in_path=path,
                                                                            out_path=os.path.splitext(path)[0])
            try:
                MiscUtils.exec_os_command(command)
                logger.info("Extracted " + path + " to " + os.path.splitext(path)[0])
            except Exception as e:
                logger.error("Error when extracting " + path + " Exeption thrown: " + str(e))
            extract_from_archives(dlt_dir)

    logger.info("Creating logfiles and core_dumps from dlts...")
    initial_and_extracted_files = MiscUtils.get_fs_tree_from_path(dlt_dir)

    valid_dlt_dir = os.path.join(dlt_dir, "ValidDLTs")
    if not os.path.exists(valid_dlt_dir):
        os.makedirs(os.path.join(valid_dlt_dir))

    are_acore_or_linux_dlts = False
    for path in initial_and_extracted_files:
        path_ext = path.split(".")[-1]
        if ("acore" in path.lower()) or ("linux" in path.lower()):
            are_acore_or_linux_dlts = True
            logger.info("Found ACORE or LINUX files... we would only consider ACORE dlts to parse...")
            break


    for path in initial_and_extracted_files:
        path_ext = path.split(".")[-1]
        should_parse_it = path_ext.lower() == "dlt" and not os.path.isdir(path)
        if are_acore_or_linux_dlts == True:
            should_parse_it = should_parse_it and (("acore" in path.lower()) or ("linux" in path.lower()))
        if should_parse_it:
            try:
                logger.info("Moving valid DLT file to processing dir..")
                shutil.move(path, valid_dlt_dir)
            except Exception as e:
                logger.info("Error when moving " + path + ".Exception thrown: " + str(e))
        elif not os.path.isdir(path):
            os.remove(path)
            logger.info("Removed " + path)

    try:
        create_core_dumps_drom_dlt_using_dobbylt(dobbylt_bin, valid_dlt_dir, output_dir)
        logger.info("Got core dumps from " + path)
    except Exception as e:
        logger.error("Error when creating core dumps from " + path + " Exeption thrown: " + str(e))

# Method used to parse .dlt files with the purpose of creating logfiles and core dumps
def create_logfiles_and_core_dumps_from_dlts(dlt_dir, core_dumps_dir, logs_dir, dlt_viewer_bin, dlt_filter, ticket_id):

    TicketRepo.Instance().updateTicketStatus(ticket_id, TicketStatus.proc_attch.value)

    # first, make sure there are no spaces in input file names
    initial_files = MiscUtils.get_fs_tree_from_path(dlt_dir)
    for path in initial_files:
        filename = os.path.basename(path)
        dirname = os.path.dirname(path)
        if ' ' in filename:
            filename = filename.replace(' ', '_')
            try:
                os.rename(path, os.path.join(dirname, filename))
            except Exception as e:
                logger.error(str(e))

    logger.info("Searching for archived files in " + dlt_dir)
    extract_from_archives(dlt_dir)

    initial_files = MiscUtils.get_fs_tree_from_path(dlt_dir)
    for path in initial_files:
        if MiscUtils.is_splitted_archive(path):
            command = "7z x \"{in_path}\" -o\"{out_path}\" -y > nul".format(in_path=path, out_path=os.path.splitext(path)[0])
            try:
                MiscUtils.exec_os_command(command)
                logger.info("Extracted " + path + " to " + os.path.splitext(path)[0])
            except Exception as e:
                logger.error("Error when extracting " + path + " Exeption thrown: " + str(e))
            extract_from_archives(dlt_dir)

    TicketRepo.Instance().updateTicketStatus(ticket_id, TicketStatus.extr_coredmp.value)
    logger.info("Creating logfiles and core_dumps from dlts...")
    initial_and_extracted_files = MiscUtils.get_fs_tree_from_path(dlt_dir)

    are_acore_or_linux_dlts = False
    for path in initial_and_extracted_files:
        path_ext = path.split(".")[-1]
        if ("acore" in path.lower()) or ("linux" in path.lower()):
            are_acore_or_linux_dlts = True
            logger.info("Found ACORE or LINUX files... we would only consider ACORE/LINUX dlts to parse...")
            break

    for path in initial_and_extracted_files:
        path_ext = path.split(".")[-1]
        should_parse_it = path_ext.lower() == "dlt" and not os.path.isdir(path)
        if are_acore_or_linux_dlts == True:
            should_parse_it = should_parse_it and (("acore" in path.lower()) or ("linux" in path.lower()))
        if should_parse_it:
            # for logfiles
            log_path = os.path.join(logs_dir, os.path.basename(os.path.splitext(path)[0] + ".txt"))
            # logger.info("LOGFILE PATH: " + log_path)
            try:
                create_logfile_from_dlt(dlt_viewer_bin, path, dlt_filter, log_path)
                logger.info("Finished parsing " + path + " to " + log_path)
            except Exception as e:
                logger.error("Error when creating logfile from " + path + " Exeption thrown: " + str(e))

            # now for core dumps
            try:
                create_core_dumps_drom_dlt(dlt_viewer_bin, path, core_dumps_dir)
                logger.info("Got core dumps from " + path) 
            except Exception as e:
                logger.error("Error when creating core dumps from " + path + " Exeption thrown: " + str(e))
        elif not os.path.isdir(path):
            dir_of_file = os.path.dirname(path)
            if len(os.listdir(dir_of_file)) == 1:
                shutil.rmtree(dir_of_file)
                logger.info("Removed " + dir_of_file)
            else:
                try:
                    os.remove(path)
                    logger.info("Removed " + path)
                except Exception as e:
                    logger.warn("Error when trying to delete " + path + " Exeption thrown: " + str(e))

def retrieve_build_for_core_dump(server_info, core_dump_logfile_data, local_downloads_dir):
    dump_name = get_core_path_for_core_dump(core_dump_logfile_data)
    version_number = get_version_number_for_core_dump(core_dump_logfile_data)
    logger.info("Attempting to connect to " + server_info["server_name"] + " and download build for " + dump_name + " with version " + version_number)

    is_connected = False
    for i in range(0, 3):
        try:  
            conn = SMBConnection(server_info["user"], server_info["password"], server_info["client_machine_name"], server_info["server_name"], use_ntlm_v2 = True)
            is_connected = conn.connect(server_info["client_machine_name"], 139)
            if is_connected:
                break
        except Exception as e:
            logger.warn("Connection error.. with exception " + repr(e) + ". Trying again..")
    logger.info("Connection result: " + str(is_connected))

    if not is_connected:
        logger.warning("Failed to connect to server " + server_info["server_name"] + " for retrieving build for " + dump_name + " with version " + version_number)
        return False

    server_root_path = server_info["server_root_path"]
    server_milestone_paths_list = server_info["server_milestone_paths_list"]
    
    server_milestone_path = ""
    server_paths_for_version = ""
    used_server_path_for_version = ""

    for this_milestone_path in server_milestone_paths_list:
        # have at look at the builds listed on server
        available_builds = []
        try:
            server_root_fs = conn.listPath(server_root_path, this_milestone_path)
            for file in server_root_fs:
                if len(file.filename) == 18:
                    available_builds.append(file.filename)
        except Exception as e:
            logger.warning("Exception raised when listing server paths for " + dump_name + " with version " + version_number + " The folder does not exist on " + server_info["server_name"] + " Exception thrown: " + repr(e))
            return False

        server_paths_for_version = "\n".join(s for s in available_builds if version_number in s)
        server_milestone_path = this_milestone_path
        if server_paths_for_version:
            logger.info("Found a possible build for " + dump_name + " with version " + version_number)
            break
       
    if not server_paths_for_version:
        logger.info("Could not found any build for " + dump_name + " with version " + version_number)
        return False

    used_server_path_for_version = server_paths_for_version.split("\n")[0]

    if len(server_paths_for_version.split("\n")) > 1:
        logger.warning("Multiple paths found for version " + version_number + ". Paths are: " + server_paths_for_version + ". Automatically selected " + used_server_path_for_version)

    try:
        # get the paths inside \build_ver\Debug 
        debug_build_folder_for_version = conn.listPath(server_root_path, os.path.join(server_milestone_path, used_server_path_for_version, "Debug"))
        # logger.info("DEBUG_BUILD_FOLDER: " + os.path.join(server_milestone_path, used_server_path_for_version, "Debug"))
    except Exception as e:
        logger.warning("Exception raised when listing server paths for " + dump_name + " with version " + version_number + " The folder does not exist on " + server_info["server_name"] + " Exception thrown: " + repr(e))
        return False

    variants_folders = []
    for file in debug_build_folder_for_version:
        variants_folders.append(file.filename)

    if is_emergency_build_for_core_dump(core_dump_logfile_data):
        # emergency builds
        variants_folders = [s for s in variants_folders if "emergency" in s]
    else:
        # normal builds
        variants_folders = [s for s in variants_folders if "emergency" not in s]

    # only Mainline builds
    variants_folders = [s for s in variants_folders if "YoctoMainline" in s]

    # for required version
    required_number, version_type = get_version_high_or_low_for_core_dump(core_dump_logfile_data)

    # gather all we have
    found_folder = "\n".join(s for s in variants_folders if required_number in s)

    if not found_folder:
        logger.info("Could not found any build for " + dump_name + " with version " + version_number)
        return False

    server_download_dir = os.path.join(server_milestone_path, used_server_path_for_version, "Debug", found_folder) 
    server_download_file = "debugzip.tar.bz2"
    server_full_download_path = os.path.join(server_download_dir, server_download_file)

    relative_build_path = server_full_download_path.split('.tar.bz2')[0]
    isEmergency = True if 'emergency' in server_download_dir else False

    if os.path.isfile(os.path.join(local_downloads_dir, server_full_download_path)):
        return True

    # logger.info("WHERE TO DOWNLOAD BUILD: " + os.path.join(local_downloads_dir, server_download_dir))
    logger.info("Begin downloading for " + dump_name + " with version " + version_number)
    try:
        file_obj = tempfile.NamedTemporaryFile()
        file_attributes, filesize = conn.retrieveFile(server_root_path, server_full_download_path, file_obj)
    except Exception as e:
        logger.error("Exception raised when downloading build for " + dump_name + " with version " + version_number + " Exception: " + repr(e))
        return False
    logger.info("Downloaded in " + server_full_download_path)

    MiscUtils.create_dir_if_not_exists(os.path.join(local_downloads_dir, server_download_dir))

    local_download_filename = os.path.join(local_downloads_dir, server_download_dir, server_download_file)

    # Write it on disk
    try:
        file_obj.seek(0)
        # logger.info("BUILD ZIP: " + local_download_filename)
        g = open(local_download_filename, "wb")
        g.write(file_obj.read())
        file_obj.close()
        g.close()
    except Exception as e:
        logger.error("Exception raised when trying to write to disk the build for " + dump_name + " with version " + version_number + " Exception: " + str(e))
        return False

    # Now extract it
    logger.info("Now attempt to extract..")
    try:
        if MiscUtils.is_os_windows():
            # since using tar from cygwin shell, properly convert paths to "/cigdrive/E/archive.tar.bz2"
            command = "tar -xvjf {archive_location} -C {out_path}".format(archive_location=MiscUtils.convert_in_cygwin_path(local_download_filename), out_path=os.path.join(MiscUtils.convert_in_cygwin_path(local_downloads_dir), server_download_dir))
            command = command.replace("\\", "/")
            MiscUtils.exec_cygwin_command(command)
        else:
            command = "tar -xjvf {archive_location} -C {out_path}".format(archive_location=local_download_filename, out_path=os.path.join(local_downloads_dir, server_download_dir))
            MiscUtils.exec_os_command(command)
    except Exception as e:
        logger.error("Exception raised when trying to extract the build for " + dump_name + " with version " + version_number + " Exception: " + str(e))
        return False
        
    logger.info("Extracted to " + local_download_filename.split(".")[0])

    BuildRepo.Instance().addBuild(version_number, required_number, relative_build_path, isEmergency)

    return True

def get_build_path_from_db(core_dump_logfile_data):
    version_number = get_version_number_for_core_dump(core_dump_logfile_data)
    required_number, version_type = get_version_high_or_low_for_core_dump(core_dump_logfile_data)

    build_root_path = BuildRepo.Instance().getBuildAbsolutePath(version_number, required_number)
    if not build_root_path:
        return []

    build_app_needed = get_build_name_for_core_dump(core_dump_logfile_data)

    build_folders = MiscUtils.get_fs_tree_from_path(build_root_path)
    possible_build_apps = [s for s in build_folders if build_app_needed in s]

    return possible_build_apps

# Method uses gdb debugger to create the backtraces, having the builds and the core dumps
def create_bts_and_check_if_kernel_panic_exists(log_data, build_dir, bt_dir, core_dumps_path, gdb_path_for_build_version_high, gdb_path_for_build_version_low, servers_info, ticket_id, extended_backtrace):
    logger.info("Now creating the backtraces..")

    response = []
    TicketRepo.Instance().updateTicketStatus(ticket_id, TicketStatus.create_bt.value)
    # first, check if any core dump with kernel-panic name exists
    kernel_panic_dir = os.path.join(core_dumps_path, "kernel-panic")
    kernel_panic_files = MiscUtils.get_fs_tree_from_path(kernel_panic_dir)
    for file in kernel_panic_files:
        if "console" in file:
            logger.info("Found kernel panic bt !")
            kernel_bt = ""
            try:
                with open(file, 'r') as content_file:
                    kernel_bt = content_file.read()
            except Exception as e:
                logger.warn("failed to read {file}".format(file=file))
            payload = {}
            payload["crash_id"] = "KERNEL_PANIC_{ticket_id}_{hash}".format(ticket_id = str(ticket_id), hash = hashlib.md5(kernel_bt.encode()).hexdigest())
            response.append(
                TicketJobResultResponse(payload=payload, bt=kernel_bt, success=True, errorMessage=""))

    if len(log_data) == 0:
        logger.info("No core dumps found in dlts for creating backtraces!")

    for log_line in log_data:
        version_number = get_version_number_for_core_dump(log_line)
        core_dump_path = os.path.join(core_dumps_path, get_core_path_for_core_dump(log_line))
        # logger.info("CORE DUMP PATH: " + core_dump_path)

        can_create_bt = False

        build_paths = get_build_path_from_db(log_line)
        if build_paths:
            can_create_bt = True
        else:
            for server_info in servers_info:
                if can_create_bt == True:
                    break
                TicketRepo.Instance().updateTicketStatus(ticket_id, TicketStatus.dld_build.value)
                attempt_download_build = retrieve_build_for_core_dump(server_info, log_line, build_dir)

                if attempt_download_build == True:
                    build_paths = get_build_path_from_db(log_line)

                    if build_paths:
                        can_create_bt = True

        if can_create_bt == True:
            bt_path = os.path.join(bt_dir, get_core_path_for_core_dump(log_line) + ".txt")
            # logger.info("BACKTRACE PATH: " + bt_path)

            # use appropriate gdb for build version
            required_number, version_type = get_version_high_or_low_for_core_dump(log_line)
            if version_type == "High":
                logger.info("Found the appropriate build for core dump " + core_dump_path + " with version " + version_number + ". Using GDB set for build version high. Now creating bt... ")
                get_bt_from_core_dump_and_build(gdb_path_for_build_version_high, core_dump_path, build_paths[0], bt_path, extended_backtrace)
            else:
                logger.info("Found the appropriate build for core dump " + core_dump_path + " with version " + version_number + ". Using GDB set for build version low. Now creating bt... ")
                get_bt_from_core_dump_and_build(gdb_path_for_build_version_low, core_dump_path, build_paths[0], bt_path, extended_backtrace)

            bt_text_file = ""
            try:
                with open(bt_path, 'r') as content_file:
                    bt_text_file = content_file.read()
            except Exception as e:
                logger.warn("failed to read {file}".format(file = bt_path))

            response.append(TicketJobResultResponse(payload = log_line['Payload'], bt = bt_text_file, success = True, errorMessage = ""))
        else:
            errorMessage = "No appropriate build found for core dump {core_dump_path} with version {version_number}".format(core_dump_path = core_dump_path, version_number = version_number)
            response.append(TicketJobResultResponse(payload = log_line['Payload'], bt = "No appropriate build found for {app} !".format(app=get_build_name_for_core_dump(log_line))))
            logger.info(errorMessage)

    return response

def get_core_path_for_core_dump(core_dump):
    core_dump_path = core_dump['Payload']['core_path'].split("/")[-1]
    return core_dump_path.split(".")[0]

def get_version_number_for_core_dump(core_dump):
    tmp = core_dump['Payload']['version'].replace('.', '_').split('_')
    for x in tmp:
        if len(x) == 6:
            return x
    return "No version in format AAAAA1 found"

def get_version_high_or_low_for_core_dump(core_dump):
    version_type = core_dump['Payload']['version']
    variant = core_dump['Payload']['variant']
    if variant == "":
        variant = "273"
        tmp = version_type.replace('.', '_').split('_')
        for x in tmp:
            if x == "High":
                variant = "278"
                break
    else:
        variant = ''.join(filter(lambda x: x.isdigit(), variant))
    core_dump['Payload']['variant'] = variant
    return (variant, version_type)

def get_build_name_for_core_dump(core_dump):
    if MiscUtils.is_os_windows():
        return core_dump['Payload']['full_path'].replace("!", "\\")
    return core_dump['Payload']['full_path'].replace("!", "/")

def is_emergency_build_for_core_dump(core_dump):
    emergency_apps = ["swdl"]
    full_build_name = get_build_name_for_core_dump(core_dump)
    if any(emergency_app.lower() in full_build_name.lower() for emergency_app in emergency_apps):
        return True
    return False

def get_bt_from_core_dump_and_build(gdb_path, core_dump_path, build_path, bt_output_path, extended_backtrace):
    build_path = build_path.replace("\\", "/")
    build_os_files = os.path.join(build_path.split("debugzip", 1)[0], "debugzip")

    command = ""
    if extended_backtrace == False:
        command = "\"{gdb_path}\" \"{build_path}\" \"{core_dump_path}\" -ex \"set confirm off\" -ex file -ex \"set sysroot {build_os_files}\" -ex \"info sharedlibrary\" -ex \"file {build_path}\" -ex \"info threads\" -ex bt -ex quit 2>&1 | tee \"{bt_output_path}\"".format(
            gdb_path=gdb_path, build_path=build_path, core_dump_path=core_dump_path, build_os_files=build_os_files,
            bt_output_path=bt_output_path)
    else:
        command = "\"{gdb_path}\" \"{build_path}\" \"{core_dump_path}\" -ex \"set confirm off\" -ex file -ex \"set sysroot {build_os_files}\" -ex \"info sharedlibrary\" -ex \"file {build_path}\" -ex \"info threads\" -ex \"thread apply all bt\" -ex quit 2>&1 | tee \"{bt_output_path}\"".format(
            gdb_path=gdb_path, build_path=build_path, core_dump_path=core_dump_path, build_os_files=build_os_files,
            bt_output_path=bt_output_path)

    try:
        if MiscUtils.is_os_windows():
            command = command.replace("\\", "/")
            MiscUtils.exec_cygwin_command(command)
        else:
            MiscUtils.exec_os_command(command)
    except RuntimeError as e:
        logger.error(e) 

    logger.info("gdb command: " + command)

########################################################################################################################

def start_getting_the_backtraces(INPUT_DLT_VIEWER_PATH,
                                 INPUT_DOBBYLT_PATH,
                                 INPUT_DLT_VIEWER_FILTER,
                                 INPUT_GDB_PATH_FOR_BUILD_VERSION_HIGH,
                                 INPUT_GDB_PATH_FOR_BUILD_VERSION_LOW,
                                 INPUT_DLT_FILES_LOCATION,
                                 INPUT_TICKET_ROOT_LOCATION,
                                 INPUT_CORE_DUMPS_LOCATION,
                                 INPUT_BUILDS_LOCATION,
                                 INPUT_BACKTRACES_LOCATION,
                                 INPUT_LOGFILES_LOCATION,
                                 INPUT_SERVERS_INFO,
                                 INPUT_TICKET_ID,
                                 INPUT_OPTIONS):

    DLT_FILES_LOCATION   = INPUT_DLT_FILES_LOCATION
    TICKET_ROOT_LOCATION = INPUT_TICKET_ROOT_LOCATION
    CORE_DUMPS_LOCATION  = INPUT_CORE_DUMPS_LOCATION
    BUILDS_LOCATION      = INPUT_BUILDS_LOCATION
    BACKTRACES_LOCATION  = INPUT_BACKTRACES_LOCATION
    LOGFILES_LOCATION    = INPUT_LOGFILES_LOCATION
    OUPTUT_ROOT_LOCATION = os.path.dirname(CORE_DUMPS_LOCATION)

    DLT_VIEWER_PATH     = INPUT_DLT_VIEWER_PATH
    DLT_VIEWER_FILTER   = INPUT_DLT_VIEWER_FILTER
    DOBBYLT_PATH        = INPUT_DOBBYLT_PATH

    GDB_PATH_FOR_BUILD_VERSION_LOW  = INPUT_GDB_PATH_FOR_BUILD_VERSION_LOW
    GDB_PATH_FOR_BUILD_VERSION_HIGH = INPUT_GDB_PATH_FOR_BUILD_VERSION_HIGH

    SERVERS_INFO = INPUT_SERVERS_INFO

    TICKET_ID = INPUT_TICKET_ID

    MAKE_EXTENDED_BACKTRACE = INPUT_OPTIONS["extendedBacktrace"]
    PROCESS_FULL_PATH = INPUT_OPTIONS["processFullPath"]
    VARIANT = INPUT_OPTIONS["variant"]

    global logger
    logger = LoggerFactory.getFactoryLogger("CrashParser", os.path.join(TICKET_ROOT_LOCATION, "btExtractorTmp"), "backtraceExtractor")

    ########################################################################################################################

    create_logfiles_and_core_dumps_from_dlts(DLT_FILES_LOCATION, CORE_DUMPS_LOCATION, LOGFILES_LOCATION, DLT_VIEWER_PATH, DLT_VIEWER_FILTER, TICKET_ID)

    # create_logfiles_and_core_dumps_from_dlts_using_dobbylt(DLT_FILES_LOCATION, OUPTUT_ROOT_LOCATION, DOBBYLT_PATH)

    extract_core_dumps(CORE_DUMPS_LOCATION)

    log_data = parse_logfiles(LOGFILES_LOCATION, PROCESS_FULL_PATH, VARIANT)

    return create_bts_and_check_if_kernel_panic_exists(log_data, BUILDS_LOCATION, BACKTRACES_LOCATION, CORE_DUMPS_LOCATION, GDB_PATH_FOR_BUILD_VERSION_HIGH, GDB_PATH_FOR_BUILD_VERSION_LOW, SERVERS_INFO, TICKET_ID, MAKE_EXTENDED_BACKTRACE)
