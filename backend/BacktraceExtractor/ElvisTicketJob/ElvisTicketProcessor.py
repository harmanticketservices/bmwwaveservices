import argparse, os, subprocess, logging, yaml, copy

######################################################################################################################

from Globals import *
from TicketDownloaderConfig import *
from Singleton import Singleton

######################################################################################################################

import MiscUtils, LoggerFactory, CrashParser

######################################################################################################################

@Singleton
class ElvisTicketProcessor:

    def __init__(self):
       self.setupPaths()
       self.setupServerCredentials()

    def setupPaths(self):
        self.dltViewerFilter    = BACKTRACE_EXTRACTOR_TOOLS + r"/dlt_crash_filter.dlf"
        self.targetsLocation 	= BACKTRACE_EXTRACTOR_BUILDS + r"/targets"

        if MiscUtils.is_os_windows():
            self.dltViewerPath               = BACKTRACE_EXTRACTOR_TOOLS + r"/dlt_viewer/dlt_viewer.exe"
            self.dobbyLTPath                 = BACKTRACE_EXTRACTOR_TOOLS + r"/DobbyLT/DobbyLT.exe"
            self.gdbPathForTargetVersionHigh = BACKTRACE_EXTRACTOR_TOOLS + r"/gcc-linaro-arm-linux-gnueabihf-4.8-2013.10_win32/bin/arm-linux-gnueabihf-gdb.exe"
            self.gdbPathForTargetVersionLow  = BACKTRACE_EXTRACTOR_TOOLS + r"/gcc-linaro-arm-linux-gnueabihf-4.8-2013.10_win32/bin/arm-linux-gnueabihf-gdb.exe"
        else:
            self.dltViewerPath               = r"/usr/bin/dlt_viewer"
            self.gdbPathForTargetVersionHigh = r"/opt/elina/2.0.2015143A/sysroots/x86_64-elinasdk-linux/usr/bin/aarch64-elina-linux/aarch64-elina-linux-gdb"
            self.gdbPathForTargetVersionLow  = r"/opt/elina/2.0.2015143A/sysroots/x86_64-elinasdk-linux/usr/bin/aarch64-elina-linux/aarch64-elina-linux-gdb"
            #self.gdbPathForTargetVersionLow  = r"/Kernel44/deliveries/ram/b273/sysroots/x86_64-elinasdk-linux/usr/bin/arm-elina-linux-gnueabi/arm-elina-linux-gnueabi-gdb"
            #self.gdbPathForTargetVersionHigh = r"/Kernel44/deliveries/ram/b278/sysroots/x86_64-elinasdk-linux/usr/bin/arm-elina-linux-gnueabi/arm-elina-linux-gnueabi-gdb"

    def create_server_info(self, user, password, client_machine_name, server_name, server_root_path, server_milestone_paths_list):
        server_info 							   = {}
        server_info["user"] 					   = user
        server_info["password"] 				   = password
        server_info["client_machine_name"] 	       = client_machine_name
        server_info["server_name"] 			       = server_name
        server_info["server_root_path"]		       = server_root_path
        server_info["server_milestone_paths_list"] = server_milestone_paths_list
        return server_info

    def setupServerCredentials(self):
        self.serversInfo = []

        self.serversInfo.append(self.create_server_info(
            "",
            "",
            "10.70.51.13",
            "himdwsfs11",
            r"BMW_IuK_RAM",
            [r"10_System-Integration/I390-ATS-Offiziell",
             r"10_System-Integration/I390-Offiziell",
             r"10_System-Integration/I400-Offiziell",
             r"10_System-Integration/I400-Topc-HDCP-Activation",
             r"10_System-Integration/I410-Offiziell",
             r"10_System-Integration/I415-Offiziell",
             r"10_System-Integration/I420-Offiziell",
             r"10_System-Integration/PU-0718/I-420",
             r"10_System-Integration/PU-0718/I-425",
             r"10_System-Integration/PU-0718/I-490",
             r"10_System-Integration/PU-0319/I-410",
             r"10_System-Integration/PU-0319/I-420",
             ]
        ))

        for server_info in self.serversInfo:
            server_info["user"] 	   = USERNAME               
            server_info["password"]    = PASSWORD

    def checkAllToolsArePresent(self):
        # Check DLT Viewer binary
        if os.path.isfile(self.dltViewerPath) == False:
            raise Exception("dlt_viewer cannot be found at " + self.dltViewerPath)

        # Check DLT Viewer Filter
        if os.path.isfile(self.dltViewerFilter) == False:
            raise Exception("dlt_viewer filter cannot be found at " + self.dltViewerFilter)

        # Check GDB binaries to exist 
        if os.path.isfile(self.gdbPathForTargetVersionHigh) == False:
            raise Exception("gdb for target high version cannot be found at " + self.gdbPathForTargetVersionHigh)
        if os.path.isfile(self.gdbPathForTargetVersionLow) == False:
            raise Exception("gdb for target low version cannot be found at " + self.gdbPathForTargetVersionLow)

        # CHECK if 7zip is available
        if not MiscUtils.is_os_windows():
            # On Linux it's straight-forward..
            if not MiscUtils.is_app_installed("7z"):
                raise Exception("7z is not available in shell")
        else:
            # Assuming that you ran _setup_win.cmd, 7zip should be installed but not added to PATH.
            # So we add it to PATH for this session
            win_7zip_install_path = r"C:\Program Files\7-Zip"
            win_7zip_binary = "7z.exe"
            if not os.path.isfile(os.path.join(win_7zip_install_path, win_7zip_binary)):
                raise Exception("7z is not installed! Run _setup_win.cmd")
            # Now add it to path
            if win_7zip_install_path not in os.environ['PATH']:
                os.environ['PATH'] += ';' + win_7zip_install_path
                # Check again now
                if not MiscUtils.is_app_installed("7z"):
                    raise Exception("7z is not available in shell")

        # CHECK if Cygwin is present on Windows. If not Windows, get env variables
        if MiscUtils.is_os_windows():
            if not MiscUtils.is_cigwin_present():
                raise Exception("Cygwin not found!")
        else:
            #because it's linux we need to source the file /opt/elina/2.0.2015143A/environment-setup-aarch64-elina-linux
            subprocess.call(r"/opt/elina/2.0.2015143A/environment-setup-aarch64-elina-linux", shell=True)

    def startProcessing(self, ticketId, options, ticketLocation, backtracesLocation):

        self.coreDumpsLocation  = os.path.join(ticketLocation, "btExtractorTmp", "core_dumps")
        self.logfilesLocation 	= os.path.join(ticketLocation, "btExtractorTmp", "logfiles")
        self.attachementsLocation = os.path.join(ticketLocation, "attaches")
        try:
            MiscUtils.remove_dir(self.logfilesLocation)
        except Exception as e:
            pass

        try:
            MiscUtils.remove_dir(self.coreDumpsLocation)
        except Exception as e:
            pass

        MiscUtils.create_dir_if_not_exists(self.coreDumpsLocation)
        MiscUtils.create_dir_if_not_exists(self.targetsLocation)
        MiscUtils.create_dir_if_not_exists(backtracesLocation)
        MiscUtils.create_dir_if_not_exists(self.logfilesLocation)

        return CrashParser.start_getting_the_backtraces(self.dltViewerPath,
                                                 self.dobbyLTPath,
                                                 self.dltViewerFilter, 
                                                 self.gdbPathForTargetVersionHigh, 
                                                 self.gdbPathForTargetVersionLow,
                                                 self.attachementsLocation,
                                                 ticketLocation,
                                                 self.coreDumpsLocation, 
                                                 self.targetsLocation, 
                                                 backtracesLocation, 
                                                 self.logfilesLocation, 
                                                 self.serversInfo,
                                                 ticketId,
                                                 options)
