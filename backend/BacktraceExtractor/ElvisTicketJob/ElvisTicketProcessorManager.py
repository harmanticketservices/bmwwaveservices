from threading import Thread
import os, requests, jsonpickle, time

import LoggerFactory, MiscUtils

from Globals import *
from TicketDownloaderConfig import *
from Singleton import Singleton
from TicketDownloader import TicketDownloader
from ElvisTicketProcessor import ElvisTicketProcessor
from TicketJobRepo import TicketJobRepo
from CrashRepo import CrashRepo
from TicketRepo import TicketRepo, TicketStatus
from WebException import WebException

@Singleton
class ElvisTicketProcessorManager:

    def __init__(self):
       self.logger = LoggerFactory.getFactoryLogger("ElvisTicketProcessorManager", WORKING_DIR, "server")
       self.serviceEnabled = True
       self.consumerThread = Thread(name = "Tickets Consumer Thread", target = self.processJobs)
       self.consumerThread.start()

    def disableService(self):
        self.serviceEnabled = False
        self.logger.info("Disabling service.. Joining threads...")
        self.consumerThread.join()

    def processJobs(self):

        while self.serviceEnabled == True:
            # first erase tickets finished
            TicketJobRepo.Instance().eraseAllJobsFinished()

            # self.logger.info("Consumer thread weaked up..")
            jobList = TicketJobRepo.Instance().findAllNotFinished()
            for currentJob in jobList:
                if currentJob.isFinished == False:
                    self.logger.info("Processing job with ticket {tickedId}".format(tickedId = currentJob.ticketId))

                    downloadAttachesForTicket = os.path.join(download_root, currentJob.ticketId, "attaches")
                    MiscUtils.create_dir_if_not_exists(downloadAttachesForTicket)

                    numOfFiles = len(MiscUtils.get_fs_tree_from_path(downloadAttachesForTicket))

                    if numOfFiles == 0:
                        TicketRepo.Instance().updateTicketStatus(currentJob.ticketId, TicketStatus.dld_attch.value)
                        self.logger.info("Attempting to download attachments for ticket {ticketId}".format(ticketId = currentJob.ticketId))
                        try:
                            numOfFiles = TicketDownloader.Instance().loginAndDownload(requests.Session(), int(currentJob.ticketId), "attaches")
                        except Exception as e:
                            self.logger.warning("Could not download attachments")
                    else:
                        self.logger.info("Attachments for ticket {ticketId} are already downloaded".format(ticketId=currentJob.ticketId))

                    if numOfFiles != 0:
                        options = {}
                        options["extendedBacktrace"] = currentJob.extendedBacktrace
                        options["processFullPath"] = currentJob.processFullPath
                        options["variant"] = currentJob.variant
                        response = ElvisTicketProcessor.Instance().startProcessing(
                            currentJob.ticketId, options, os.path.join(download_root, currentJob.ticketId), os.path.join(download_root, currentJob.ticketId, "backtraces"))
                        TicketRepo.Instance().addSolvedCrashes(currentJob.ticketId, response)
                        CrashRepo.Instance().addCrashes(currentJob.ticketId, response)
                        self.logger.info("Job with ticket {tickedId} is finished!".format(tickedId = currentJob.ticketId))

                    else:
                        TicketRepo.Instance().setBacktraceErrorMessage(currentJob.ticketId, "Could not download attachments")
                        self.logger.info("Ticket {tickedId} has 0 attachments".format(tickedId = currentJob.ticketId))

                    TicketRepo.Instance().updateTicketStatus(currentJob.ticketId, TicketStatus.fin.value)
                    TicketJobRepo.Instance().updateStatus(currentJob.getId(), True)

            time.sleep(1.5)
          
    def addJob(self, ticketId, extendedBacktrace, processFullPath, variant):
        jobId = TicketJobRepo.Instance().add(str(ticketId), extendedBacktrace, processFullPath, variant)
        self.logger.info("Added job with {id} for ticket with {ticketId}".format(id = jobId, ticketId = ticketId))
        return jsonpickle.encode({"ticketId": str(ticketId)}, unpicklable = False)


    def addDeveloperJob(self,ticketId,extendedBacktrace,processFullPath,AplicationFullPath,variant):
        jobId=TicketJobRepo.Instance().addDevJob(str(ticketId),extendedBacktrace,processFullPath,AplicationFullPath,variant)
        self.logger.info("Added dev job with {id} for ticket {tickedId}".format(id=jobId,ticketId=ticketId))
        return jsonpickle.encode({"ticketId": str(ticketId)},unpicklable=False)

    def addJobAgain(self, ticketId, extendedBacktrace, processFullPath, variant):
        TicketRepo.Instance().deleteCrashesFromDb(ticketId)
        return self.addJob(ticketId, extendedBacktrace, processFullPath, variant)

    def downloadCoreDump(self, ticketId, coreDumpFilename):
        job = TicketJobRepo.Instance().findByTicketId(ticketId)
        if job != None and job.isFinished == False:
            raise WebException(message="Ticket with id {ticketId} is not processed yet!".format(ticketId=ticketId), status_code = 401)

        ticket = TicketRepo.Instance().findByTicketId(ticketId)
        if ticket == None:
            raise WebException(message="Ticket with id {ticketId} doesn't exist!".format(ticketId=ticketId), status_code=401)

        if ticket.areBacktracesSolved == False:
            raise WebException(message="Ticket with id {ticketId} is not processed yet!".format(ticketId=ticketId), status_code=401)

        archiveLocation = ""

        if coreDumpFilename == "kernel-panic":
            path = os.path.join(download_root, ticketId, "btExtractorTmp", "core_dumps", "kernel-panic.tgz")
            if not os.path.isfile(path):
                raise WebException(message="Core dump with path {path} doesn't exist!".format(path=path), status_code=401)
            archiveLocation = path
        else:
            path = os.path.join(download_root, ticketId, "btExtractorTmp", "core_dumps", coreDumpFilename)
            if not os.path.isfile(path):
                raise WebException(message="Core dump with path {path} doesn't exist!".format(path=path), status_code=401)

            archiveLocation = os.path.join(download_root, ticketId, "btExtractorTmp", "core_dumps", "{coreDumpFilename}{ext}".format(coreDumpFilename=coreDumpFilename, ext=".7z"))
            command = "7z a -t7z \"{archiveLocation}\" \"{coreDumpPath}\"".format(archiveLocation = archiveLocation, coreDumpPath = path)
            try:
                MiscUtils.exec_os_command(command)
            except Exception as e:
                raise e

            if not os.path.isfile(archiveLocation):
                raise WebException(message="Core dump {path} could not be archived".format(path=path), status_code=401)

        return archiveLocation

