from threading import Thread
import os, requests, jsonpickle, time, base64
import json

import LoggerFactory, MiscUtils

from Globals import *
from Singleton import Singleton
from CoredumpJobRepo import CoredumpJobRepo
from CoredumpRepo import CoredumpRepo
from CoredumpsJobProcessor import CoredumpsJobProcessor
from werkzeug import secure_filename
from WebException import WebException

@Singleton
class CoredumpsJobProcessorManager:
    def __init__(self):
        self.logger = LoggerFactory.getFactoryLogger("CoredumpsJobProcessorManager", WORKING_DIR, "server")
        MiscUtils.create_dir_if_not_exists(COREDUMP_JOBS_LOCATION)
        self.serviceEnabled = True
        self.consumerThread = Thread(name="Coredumps Consumer Thread", target=self.processJobs)
        self.consumerThread.start()

    def disableService(self):
        self.serviceEnabled = False
        self.logger.info("Disabling service.. Joining threads...")
        self.consumerThread.join()

    def processJobs(self):

        while self.serviceEnabled == True:
            # first erase tickets finished
            CoredumpJobRepo.Instance().eraseAllJobsFinished()

            jobList = CoredumpJobRepo.Instance().findAllNotFinished()
            for currentJob in jobList:
                if currentJob.isFinished == False:
                    self.logger.info("Processing coredumpjob {coredumpJobId}".format(coredumpJobId=currentJob.coredumpJobId))

                    response = CoredumpsJobProcessor.Instance().startProcessing(currentJob)

                    CoredumpRepo.Instance().addSolvedCrash(currentJob.coredumpJobId, response)
                    CoredumpJobRepo.Instance().updateStatus(currentJob.getId(), True)

                    self.logger.info("coredumpjob {coredumpJobId} is finished!".format(coredumpJobId=currentJob.coredumpJobId))

            time.sleep(1.5)

    def addJob(self, coredumpJobId, variant, processFullPath, buildVersion, extendedBacktrace, coreDumpFile):

        relativePath = os.path.join(coredumpJobId, "file", secure_filename(coreDumpFile.filename))
        absolutePath = os.path.join(COREDUMP_JOBS_LOCATION, relativePath)
        processFullPath = base64.b64decode(processFullPath)

        MiscUtils.create_dir_if_not_exists(os.path.join(COREDUMP_JOBS_LOCATION, coredumpJobId, "file"))
        MiscUtils.create_dir_if_not_exists(os.path.join(COREDUMP_JOBS_LOCATION, coredumpJobId, "backtrace"))

        coreDumpFile.save(absolutePath)
        jobId = CoredumpJobRepo.Instance().add(coredumpJobId, extendedBacktrace, processFullPath, variant, buildVersion, relativePath)
        CoredumpRepo.Instance().addCoredump(coredumpJobId, processFullPath, variant, buildVersion, relativePath)
        self.logger.info("Added job with {id} for coredumpJob with {coredumpJobId}".format(id=jobId, coredumpJobId=coredumpJobId))
        return jsonpickle.encode({"coredumpJobId": str(coredumpJobId)}, unpicklable=False)




    def uploadDevFile(self,devFile,FilePath,devName,buildvers):

        pathToDevToolbox="/ioio/devsandbox/"
        finishedUploadingDevFiles = False

        CoredumpsJobProcessor.Instance().setDownloadBuildStatus(finishedUploadingDevFiles)
        #FilePath and BuildVers are sent encoded as Base64, they need to be decoded
        processpath=base64.b64decode(FilePath)
        #B64Decode returns the string in byte form, so in order to use it later in string collation we have to convert it to utf-8 format string.
        utf8path=processpath.decode("utf-8")

        decodedBuildVers=base64.b64decode(buildvers)

        utf8buildvers=decodedBuildVers.decode("utf-8")


        #Check if build number exists in Developer's Sandbox
        auxPathforBuild = pathToDevToolbox + devName + "/" + utf8buildvers

        BuildFound = os.path.isdir(auxPathforBuild)

        if BuildFound:
            self.logger.info("Build {buildnr} exists for {devuser}".format(buildnr=utf8buildvers,devuser=devName))
        else:
            self.logger.info("Build not found, ensuing download")
            #wait for build download
            CoredumpsJobProcessor.Instance().downloadBuildForDevCoreDump(utf8buildvers, "tbd", "tbd",devName)

        '''
        foundBuild = None
        #foundBuild = BuildRepo.Instance().getBuildAbsolutePath(buildVersion, variant)
        if foundBuild == None:
            canCreateBt = False
            self.logger.info("COCOS: I am here 1 ")
            # Download build if none found
            for serverInfo in CoredumpsJobProcessor.Instance().serversInfo:
                if canCreateBt == True:
                    break
                attemptDownloadBuild = CoredumpsJobProcessor.Instance().downloadBuildForeCoreDump(serverInfo, utf8buildvers, "tbd", "tbd")
                if attemptDownloadBuild == True:
                    possibleFoundBuild = CoredumpsJobProcessor.Instance().BuildRepo.Instance().getBuildAbsolutePath(utf8buildvers, "tbd")
                    #possibleFoundBuild = True
                    if possibleFoundBuild:
                        canCreateBt = True
                        foundBuild = possibleFoundBuild

        '''

        pathToArch64 = ""
        done=False
        for dirpath, dirnames, filenames in os.walk(auxPathforBuild):
            if not done:
                for filename in dirnames:
                    if filename == "aarch64-elina-linux":
                        pathToArch64  = os.path.join(dirpath, filename)
                        done=True
                        break
            if done:
                break


        pathtoApplication = pathToArch64 + utf8path


        fileExists=os.path.exists(pathtoApplication)
        if(fileExists):

            command="rm -rf "+ pathtoApplication
            MiscUtils.exec_os_command(command)
        else:
            self.logger.info("Searched file not found")






        self.logger.info("{filepath} filepath".format(filepath=pathtoApplication))
        devFile.save(pathtoApplication)
        finishedUploadingDevFiles = True
        return jsonpickle.encode({"coredumpJobId": str(devName)}, unpicklable=False)

    def checkDeveloperSandbox(self,devName):
        developer_sandbox_path="/ioio/devsandbox"
        MiscUtils.create_dir_if_not_exists(os.path.join(developer_sandbox_path,devName))
        if os.path.isdir(os.path.join(developer_sandbox_path,devName)):
            return True
        else:
            return False

    def checkIfFinishedDownloading(self):

        return finishedUploadingDevFiles

    def addDevJob(self,coredumpJobId,variant,processFullPath,buildVersion,extendedBacktrace,ApplicationFullPath):

        relativePath = os.path.join(coredumpJobId, "file", secure_filename(coreDumpFile.filename))
        absolutePath = os.path.join(COREDUMP_JOBS_LOCATION, relativePath)
        processFullPath = base64.b64decode(processFullPath)
        ApplicationFullPath=base64.b64decode(ApplicationFullPath)


        MiscUtils.create_dir_if_not_exists(os.path.join(COREDUMP_JOBS_LOCATION, coredumpJobId, "file"))
        MiscUtils.create_dir_if_not_exists(os.path.join(COREDUMP_JOBS_LOCATION, coredumpJobId, "backtrace"))

        coreDumpFile.save(absolutePath)
        jobId = CoredumpJobRepo.Instance().add(coredumpJobId, extendedBacktrace, processFullPath, variant, buildVersion, relativePath)
        CoredumpRepo.Instance().addCoredump(coredumpJobId, processFullPath, variant, buildVersion, relativePath)
        self.logger.info("Added job with {id} for coredumpJob with {coredumpJobId}".format(id=jobId, coredumpJobId=coredumpJobId))
        return jsonpickle.encode({"coredumpJobId": str(coredumpJobId)}, unpicklable=False)


    def addJobAgain(self, coredumpJobId, variant, processFullPath, buildVersion, extendedBacktrace):

        coredumpData = CoredumpRepo.Instance().findByCoredumpId(coredumpJobId)
        if coredumpData == None:
            raise WebException(message="Coredump job {coredumpJobId} doesn't exist in db!".format(coredumpJobId=coredumpJobId),
                               status_code=401)

        processFullPath = base64.b64decode(processFullPath)
        jobId = CoredumpJobRepo.Instance().add(coredumpJobId, extendedBacktrace, processFullPath, variant, buildVersion,
                                               coredumpData.coredumpPath)
        CoredumpRepo.Instance().deleteCrash(coredumpJobId)
        CoredumpRepo.Instance().updateCoredump(coredumpJobId, processFullPath, variant, buildVersion)
        self.logger.info(
            "Added job with {id} for coredumpJob with {coredumpJobId}".format(id=jobId, coredumpJobId=coredumpJobId))
        return jsonpickle.encode({"coredumpJobId": str(coredumpJobId)}, unpicklable=False)

    def downloadCoreDump(self, coredumpJobId):
        coredumpData = CoredumpRepo.Instance().findByCoredumpId(coredumpJobId)
        if coredumpData == None:
            raise WebException(
                message="Coredump job {coredumpJobId} doesn't exist in db!".format(coredumpJobId=coredumpJobId),
                status_code=401)

        return os.path.join(COREDUMP_JOBS_LOCATION, coredumpData.coredumpPath)

    def getAllCoreDumpsDoneJobs(self):
        return CoredumpRepo.Instance().getAllCoreDumpsData()
    def writeToServerMilestone(self):
        with open(SERVER_CONFIGS_PATH,'w') as outfile:
            json.dump(CoredumpsJobProcessor.Instance().serversInfo[0].get("server_milestone_paths_list"),outfile)
        return True
    def addServerBranch(self,newBranch):

        decodedBranchName=base64.b64decode(newBranch)
        decodedBranchName=decodedBranchName.decode("utf-8")

        server_milestones=CoredumpsJobProcessor.Instance().serversInfo[0].get("server_milestone_paths_list")
        milestonesNo = len(server_milestones)
        iterationNo = 0
        for item in server_milestones:

            if(item != decodedBranchName):
                iterationNo = iterationNo + 1
                if(iterationNo == milestonesNo):
                    if CoredumpsJobProcessor.Instance().addServerBranch(decodedBranchName) == True:
                        self.writeToServerMilestone()
                        return jsonpickle.encode({"serverBranch": True},unpicklable= False)
                    else:
                        print("Server branch to be added does not exist")
                        return jsonpickle.encode({"serverBranch": False},unpicklable= False)

            else:
                    print("Branch is already added")
                    return jsonpickle.encode({"serverBranch": True},unpicklable= False)




