#!/usr/bin/expect -f
#!/usr/bin/bash

set app [lindex $argv 0]

spawn bash -c "sudo chmod 777 $app"
expect {
  -re ".*sword.*"
  {
    exp_send "yoctoadm\r"
  }
}
