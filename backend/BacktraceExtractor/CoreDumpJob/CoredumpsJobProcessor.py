import argparse, os, subprocess, logging, yaml, copy
import json, os, logging, tempfile, hashlib
from shutil import copyfile
from subprocess import call
from smb.SMBConnection import SMBConnection

######################################################################################################################

from Globals import *
from TicketDownloaderConfig import *
from Singleton import Singleton
from BuildRepo import BuildRepo
from CoredumpJobStatusResponse import CoredumpJobStatusResponse
from CoredumpJobResultResponse import CoredumpJobResultResponse
from filecmp import dircmp
######################################################################################################################

import MiscUtils, LoggerFactory, json, ntpath

######################################################################################################################

@Singleton
class CoredumpsJobProcessor:
    isFinishedDownloading = False
    def __init__(self):
       self.logger = LoggerFactory.getFactoryLogger("CoredumpsJobProcessor", WORKING_DIR, "server")
       self.setupPaths()
       self.setupServerCredentials()
       self.checkAllToolsArePresent()

    def setupPaths(self):
        self.targetsLocation = BACKTRACE_EXTRACTOR_BUILDS + r"/targets"

        if MiscUtils.is_os_windows():
            self.gdbPathForTargetVersionHigh = BACKTRACE_EXTRACTOR_TOOLS + r"/gcc-linaro-arm-linux-gnueabihf-4.8-2013.10_win32/bin/arm-linux-gnueabihf-gdb.exe"
            self.gdbPathForTargetVersionLow  = BACKTRACE_EXTRACTOR_TOOLS + r"/gcc-linaro-arm-linux-gnueabihf-4.8-2013.10_win32/bin/arm-linux-gnueabihf-gdb.exe"
        else:
            self.gdbPathForTargetVersionHigh = r"/opt/elina/2.0.2015143A/sysroots/x86_64-elinasdk-linux/usr/bin/aarch64-elina-linux/aarch64-elina-linux-gdb"
            self.gdbPathForTargetVersionLow  = r"/opt/elina/2.0.2015143A/sysroots/x86_64-elinasdk-linux/usr/bin/aarch64-elina-linux/aarch64-elina-linux-gdb"
            #self.gdbPathForTargetVersionLow  = r"/Kernel44/deliveries/ram/b273/sysroots/x86_64-elinasdk-linux/usr/bin/arm-elina-linux-gnueabi/arm-elina-linux-gnueabi-gdb"
            #self.gdbPathForTargetVersionHigh = r"/Kernel44/deliveries/ram/b278/sysroots/x86_64-elinasdk-linux/usr/bin/arm-elina-linux-gnueabi/arm-elina-linux-gnueabi-gdb"

    def create_server_info(self, user, password, client_machine_name, server_name, server_root_path, server_milestone_paths_list):
        server_info 							   = {}
        server_info["user"] 					   = user
        server_info["password"] 				   = password
        server_info["client_machine_name"] 	       = client_machine_name
        server_info["server_name"] 			       = server_name
        server_info["server_root_path"]		       = server_root_path
        server_info["server_milestone_paths_list"] = server_milestone_paths_list
        return server_info

    def setupServerCredentials(self):
        self.serversInfo = []

        with open(SERVER_CONFIGS_PATH) as data_file:
            server_milestones = json.load(data_file);

        self.serversInfo.append(self.create_server_info(
            "",
            "",
            "localhost",

            "/media/",
            r"share/",
            server_milestones

        ))


        for server_info in self.serversInfo:
            server_info["user"] 	   = USERNAME
            server_info["password"]    = PASSWORD










    def checkAllToolsArePresent(self):

        # Check GDB binaries to exist
        if os.path.isfile(self.gdbPathForTargetVersionHigh) == False:
            raise Exception("gdb for target high version cannot be found at " + self.gdbPathForTargetVersionHigh)
        if os.path.isfile(self.gdbPathForTargetVersionLow) == False:
            raise Exception("gdb for target low version cannot be found at " + self.gdbPathForTargetVersionLow)

        # CHECK if 7zip is available
        if not MiscUtils.is_os_windows():
            # On Linux it's straight-forward..
            if not MiscUtils.is_app_installed("7z"):
                raise Exception("7z is not available in shell")
        else:
            # Assuming that you ran _setup_win.cmd, 7zip should be installed but not added to PATH.
            # So we add it to PATH for this session
            win_7zip_install_path = r"C:\Program Files\7-Zip"
            win_7zip_binary = "7z.exe"
            if not os.path.isfile(os.path.join(win_7zip_install_path, win_7zip_binary)):
                raise Exception("7z is not installed! Run _setup_win.cmd")
            # Now add it to path
            if win_7zip_install_path not in os.environ['PATH']:
                os.environ['PATH'] += ';' + win_7zip_install_path
                # Check again now
                if not MiscUtils.is_app_installed("7z"):
                    raise Exception("7z is not available in shell")

        # CHECK if Cygwin is present on Windows
        if MiscUtils.is_os_windows():
            if not MiscUtils.is_cigwin_present():
                raise Exception("Cygwin not found!")

    def startProcessing(self, currentJob):

        # first, extract the coredump file
        coredumpArchivedPath = os.path.join(COREDUMP_JOBS_LOCATION, currentJob.coredumpPath)

        devCoredump=False;



        if MiscUtils.is_archive(coredumpArchivedPath):
            command = "7z x \"{archivePath}\" -o\"{out_path}\" -y > nul".format(archivePath=coredumpArchivedPath,
                                                                                out_path=coredumpArchivedPath.split('.')[0])
            try:
                MiscUtils.exec_os_command(command)
                self.logger.info("Extracted " + coredumpArchivedPath)
            except Exception as e:
                self.logger.error("Error when extracting " + coredumpArchivedPath + " Exeption thrown: " + str(e))

        extractedCoreDump = None
        files = MiscUtils.get_fs_tree_from_path(os.path.join(COREDUMP_JOBS_LOCATION, currentJob.coredumpJobId, "file"))
        for file in files:
            if not MiscUtils.is_archive(file):
                extractedCoreDump = file
                break

        # compute version and variant and app path
        buildVersion = None
        tmp = currentJob.buildVersion.replace('.', '_').split('_')

        goodBuildFormat = False
        for x in tmp:
            if len(x) == 6:
                buildVersion = x
                goodBuildFormat = True

        variant = ''.join(filter(lambda x: x.isdigit(), currentJob.variant))
        processFullPath = currentJob.processFullPath

        payload = {}
        payload['full_path'] = processFullPath
        payload['variant'] = variant
        payload['version'] = buildVersion

        if goodBuildFormat == False:
            return CoredumpJobResultResponse(payload=json.dumps(payload), bt="Wrong build format !")

        payload['version'] = buildVersion

        # find the appropriate build by version and variant
        canCreateBt = True
        foundBuild = None
        #foundBuild = BuildRepo.Instance().getBuildAbsolutePath(buildVersion, variant)
        if foundBuild == None:
            canCreateBt = False
            self.logger.info("COCOS: I am here 1 ")
            # Download build if none found
            for serverInfo in self.serversInfo:
                if canCreateBt == True:
                    break
                attemptDownloadBuild = self.downloadBuildForeCoreDump(serverInfo, buildVersion, variant, extractedCoreDump)
                if attemptDownloadBuild == True:
                    possibleFoundBuild = BuildRepo.Instance().getBuildAbsolutePath(buildVersion, variant)
                    #possibleFoundBuild = True
                    if possibleFoundBuild:
                        canCreateBt = True
                        foundBuild = possibleFoundBuild
        if canCreateBt == False:
            return CoredumpJobResultResponse(payload=json.dumps(payload),
                                             bt="No appropriate build with version {version} and variant {variant} found for {app} !".format(version=buildVersion, variant=variant,
                                                 app=processFullPath))


        buildFiles = MiscUtils.get_fs_tree_from_path(foundBuild)
        possibleBuildApps = [s for s in buildFiles if processFullPath in s]
        if not possibleBuildApps:
            return CoredumpJobResultResponse(payload=json.dumps(payload),
                                                    bt="No {app} application found for build {version} !".format(version=buildVersion, app=processFullPath))
        possibleBuildApps = possibleBuildApps[0]
        btPath = os.path.join(COREDUMP_JOBS_LOCATION, currentJob.coredumpJobId, "backtrace", os.path.basename(extractedCoreDump) + ".txt")

        self.logger.info(
            "Found the appropriate build for core dump " + extractedCoreDump + " with version " + buildVersion + ". Now creating bt... ")

        buildPath = foundBuild.replace("\\", "/")
        buildRoot = buildPath#os.path.join(buildPath.split("debugzip", 1)[0], "debugzip")

        command = ""
        if currentJob.extendedBacktrace == False:
            command = "\"{gdb_path}\" \"{build_path}\" \"{core_dump_path}\" -ex \"set confirm off\" -ex \"set sysroot {build_os_files}\" -ex \"info sharedlibrary\" -ex \"info threads\" -ex bt -ex quit 2>&1 | tee \"{bt_output_path}\"".format(gdb_path=self.gdbPathForTargetVersionHigh, build_path=possibleBuildApps, core_dump_path=extractedCoreDump, build_os_files=buildRoot,bt_output_path=btPath)
            self.logger.info("{comm}".format(comm=command))
        else:
            command = "\"{gdb_path}\" \"{build_path}\" \"{core_dump_path}\" -ex \"set confirm off\" -ex \"set sysroot {build_os_files}\" -ex \"info sharedlibrary\" -ex \"info threads\" -ex \"thread apply all bt\" -ex quit 2>&1 | tee \"{bt_output_path}\"".format(gdb_path=self.gdbPathForTargetVersionHigh, build_path=possibleBuildApps, core_dump_path=extractedCoreDump, build_os_files=buildRoot,bt_output_path=btPath)
        try:
            if MiscUtils.is_os_windows():
                command = command.replace("\\", "/")
                MiscUtils.exec_cygwin_command(command)
            else:
                MiscUtils.exec_os_command(command)
        except RuntimeError as e:
            self.logger.error(e)

        self.logger.info("gdb command: " + command)

        btTextContent = ""
        try:
            with open(btPath, 'r') as contentFile:
                btTextContent = contentFile.read()
        except Exception as e:
            self.logger.warn("failed to read {file}".format(file=btPath))

        return CoredumpJobResultResponse(payload = json.dumps(payload), bt = btTextContent, success = True, errorMessage = "")

    def downloadBuildForeCoreDump(self, serverInfo, buildVersion, variant, extractedCoreDump):
        self.logger.info("Attempting to connect to " + serverInfo[
            "server_name"] + " and download build for " + extractedCoreDump + " with version " + buildVersion)
       # self.logger.info("COCOS: server_root_path " + serverInfo["server_root_path"] + " server_milestone_paths_list" + serverInfo["server_milestone_paths_list"])
        isEmergency = False
        '''
        for i in range(0, 3):
            
            try:
                conn = SMBConnection(serverInfo["user"], serverInfo["password"], serverInfo["client_machine_name"],
                                     serverInfo["server_name"], use_ntlm_v2=True)
                isConnected = conn.connect(serverInfo["client_machine_name"], 139)
                if isConnected:
                    break
            except Exception as e:
                self.logger.warn("Connection error.. with exception " 
                + repr(e) + ". Trying again..")
   
        self.logger.info("Connection result: " + str(isConnected))

        if not isConnected:
            self.logger.warning("Failed to connect to server " + self.server_info[
                "server_name"] + " for retrieving build for " + extractedCoreDump + " with version " + buildVersion)
            return False
        '''
        serverRootPath = serverInfo["server_name"] + serverInfo["server_root_path"]

        serverMilestonesPathList = serverInfo["server_milestone_paths_list"]


        debugBuildFolderForVersion = ""
        localDownloadPath = ""
        foundExactBuild=False
        for thisMilestonePath in serverMilestonesPathList:
            # have at look at the builds listed on server
            availableBuilds = []
            if not foundExactBuild:
                try:
                    serverRootFiles = os.listdir(serverRootPath + thisMilestonePath) #conn.listPath(serverRootPath, thisMilestonePath)
                    for file in serverRootFiles:
                        if file == buildVersion:
                            debugBuildFolderForVersion = os.path.join(serverRootPath, thisMilestonePath, buildVersion, "sdk")
                            foundExactBuild = True
                            break
                except Exception as e:
                    self.logger.warning(
                        "Exception raised when listing server paths for " + extractedCoreDump + " with version " + buildVersion + " The folder does not exist on " +
                        self.server_info["server_name"] + " Exception thrown: " + repr(e))
                    return False
        if not debugBuildFolderForVersion:
            self.logger.info("Could not found any build for " + extractedCoreDump + " with version " + buildVersion)
            return False

        serverDownloadFile = ""
        serverFullDownloadPath = ""
        for file in os.listdir(debugBuildFolderForVersion):
            if file.endswith(".sh"):
                serverDownloadFile = file
                serverFullDownloadPath = os.path.join(debugBuildFolderForVersion, serverDownloadFile)
                break

        if not serverDownloadFile:
            self.logger.info("Could not found any debug for " + extractedCoreDump + " in build version " + buildVersion)
            return False

        self.logger.info("Begin downloading for " + extractedCoreDump + " with version " + buildVersion)

        try:
            if not os.path.isdir(os.path.join(self.targetsLocation, thisMilestonePath)):
                os.makedirs(os.path.join(self.targetsLocation, thisMilestonePath))
            if not os.path.isdir(os.path.join(self.targetsLocation, thisMilestonePath, buildVersion)):
                os.makedirs(os.path.join(self.targetsLocation, thisMilestonePath, buildVersion))
            if not os.path.isdir(os.path.join(self.targetsLocation, thisMilestonePath, buildVersion, "sdk")):
                os.makedirs(os.path.join(self.targetsLocation, thisMilestonePath,  buildVersion, "sdk"))

            localDownloadPath = os.path.join(self.targetsLocation, thisMilestonePath, buildVersion, "sdk", serverDownloadFile)
            if not os.path.isfile(localDownloadPath):
                copyfile(serverFullDownloadPath, localDownloadPath)
        except Exception as e:
                self.logger.warning("Exception raised when listing server paths for " + extractedCoreDump + " with version " + buildVersion + " The folder was not created or file not copied Exception thrown: " + repr(e))

        sdkPath = os.path.join(self.targetsLocation, thisMilestonePath, buildVersion, "sdk", "build")
        command ="chmod 777 " + localDownloadPath

        self.logger.warning("COCOS: command is: " + command)
        MiscUtils.exec_os_command(command)

        if not os.path.isdir(sdkPath):
            command = localDownloadPath +  " -d " + sdkPath + " -y"
            self.logger.warning("COCOS: started unpacking sdk")
            MiscUtils.exec_os_command(command)

        self.logger.warning("COCOS: finished unpacking sdk")
        #wait for it to finish, e.t.a 5 mins
        appPath = os.path.join(sdkPath, "sysroots", "aarch64-elina-linux")
        BuildRepo.Instance().addBuild(buildVersion, variant, appPath, isEmergency)

        return True

    def setDownloadBuildStatus(self,status):
        self.isFinishedDownloading = status
        return self.isFinishedDownloading

    def getDownloadBuildStatus(self):
        return self.isFinishedDownloading

    def addServerBranch(self,serverBranch):
        possibleBuildPath = "/media/share/" + serverBranch
        if os.path.isdir(possibleBuildPath) == True:
            print("Branch to be added exists")
            self.serversInfo[0].get("server_milestone_paths_list").append(serverBranch)
            return True
        else:
            print("Branch to be added was not found, try again")
            return False

    def downloadBuildForDevCoreDump(self, buildVersion, variant, extractedCoreDump,devName):
        for serverInfo in self.serversInfo:
            self.logger.info("Attempting to connect to " + serverInfo[
                "server_name"] + " and download build for " + extractedCoreDump + " with version " + buildVersion)
           # self.logger.info("COCOS: server_root_path " + serverInfo["server_root_path"] + " server_milestone_paths_list" + serverInfo["server_milestone_paths_list"])
            isEmergency = False
            '''
            for i in range(0, 3):
                
                try:
                    conn = SMBConnection(serverInfo["user"], serverInfo["password"], serverInfo["client_machine_name"],
                                         serverInfo["server_name"], use_ntlm_v2=True)
                    isConnected = conn.connect(serverInfo["client_machine_name"], 139)
                    if isConnected:
                        break
                except Exception as e:
                    self.logger.warn("Connection error.. with exception " 
                    + repr(e) + ". Trying again..")
           
            self.logger.info("Connection result: " + str(isConnected))
    
            if not isConnected:
                self.logger.warning("Failed to connect to server " + self.server_info[
                    "server_name"] + " for retrieving build for " + extractedCoreDump + " with version " + buildVersion)
                return False
            '''
            serverRootPath = serverInfo["server_name"] + serverInfo["server_root_path"]

            serverMilestonesPathList = serverInfo["server_milestone_paths_list"]


            debugBuildFolderForVersion = ""
            localDownloadPath = ""
            foundExactBuild = False
            for thisMilestonePath in serverMilestonesPathList:
                # have at look at the builds listed on server
                availableBuilds = []
                if not foundExactBuild:
                    try:
                        serverRootFiles = os.listdir(serverRootPath + thisMilestonePath) #conn.listPath(serverRootPath, thisMilestonePath)
                        for file in serverRootFiles:
                            if file == buildVersion:
                                debugBuildFolderForVersion = os.path.join(serverRootPath, thisMilestonePath, buildVersion, "sdk")
                                foundExactBuild = True
                                break
                    except Exception as e:
                        self.logger.warning(
                            "Exception raised when listing server paths for " + extractedCoreDump + " with version " + buildVersion + " The folder does not exist on " +
                            self.server_info["server_name"] + " Exception thrown: " + repr(e))
                        return False
            if not debugBuildFolderForVersion:
                self.logger.info("Could not found any build for " + extractedCoreDump + " with version " + buildVersion)
                return False

            serverDownloadFile = ""
            serverFullDownloadPath = ""
            for file in os.listdir(debugBuildFolderForVersion):
                if file.endswith(".sh"):
                    serverDownloadFile = file
                    serverFullDownloadPath = os.path.join(debugBuildFolderForVersion, serverDownloadFile)
                    break

            if not serverDownloadFile:
                self.logger.info("Could not found any debug for " + extractedCoreDump + " in build version " + buildVersion)
                return False

            self.logger.info("Begin downloading for " + extractedCoreDump + " with version " + buildVersion)

            try:
                if not os.path.isdir(os.path.join(self.targetsLocation, thisMilestonePath)):
                    os.makedirs(os.path.join(self.targetsLocation, thisMilestonePath))
                if not os.path.isdir(os.path.join(self.targetsLocation, thisMilestonePath, buildVersion)):
                    os.makedirs(os.path.join(self.targetsLocation, thisMilestonePath, buildVersion))
                if not os.path.isdir(os.path.join(self.targetsLocation, thisMilestonePath, buildVersion, "sdk")):
                    os.makedirs(os.path.join(self.targetsLocation, thisMilestonePath,  buildVersion, "sdk"))

                localDownloadPath = os.path.join(self.targetsLocation, thisMilestonePath, buildVersion, "sdk", serverDownloadFile)
                if not os.path.isfile(localDownloadPath):
                    copyfile(serverFullDownloadPath, localDownloadPath)
            except Exception as e:
                    self.logger.warning("Exception raised when listing server paths for " + extractedCoreDump + " with version " + buildVersion + " The folder was not created or file not copied Exception thrown: " + repr(e))

            sdkPath = os.path.join(self.targetsLocation, thisMilestonePath, buildVersion, "sdk", "build")
            PathToSandbox = "/ioio/devsandbox/"
            devsdkPath=PathToSandbox + devName + "/" + buildVersion

            if(os.path.isdir(devsdkPath)):
                os.makedirs(devsdkPath)

            command ="chmod 777 " + localDownloadPath

            self.logger.warning("COCOS: command is: " + command)
            MiscUtils.exec_os_command(command)

            if not os.path.isdir(devsdkPath):
                command = localDownloadPath +  " -d " + devsdkPath + " -y"
                self.logger.warning("COCOS: started unpacking sdk")
                MiscUtils.exec_os_command(command)

            self.logger.warning("COCOS: finished unpacking sdk")
            #wait for it to finish, e.t.a 5 mins
            appPath = os.path.join(sdkPath, "sysroots", "aarch64-elina-linux")
            BuildRepo.Instance().addBuild(buildVersion, variant, appPath, isEmergency)

            return True



#               variantsFolders.append(file.filename)
#             serverPathsForVersion = "\n".join(s for s in availableBuilds if buildVersion in s)
#            serverMilestonePath = thisMilestonePath
#             if serverPathsForVersion:
#                 self.logger.info("Found a possible build for " + extractedCoreDump + " with version " + buildVersion)
#                 break
#
#         if not serverPathsForVersion:
#             self.logger.info("Could not found any build for " + extractedCoreDump + " with version " + buildVersion)
#             return False
#
#         usedServerPathForVersion = serverPathsForVersion.split("\n")[0]
#
#         if len(serverPathsForVersion.split("\n")) > 1:
#             self.logger.warning(
#                 "Multiple paths found for version " + buildVersion + ". Paths are: " + serverPathsForVersion + ". Automatically selected " + usedServerPathForVersion)
#
#         try:
#             # get the paths inside \build_ver\Debug
#             debugBuildFolderForVersion = conn.listPath(serverRootPath, os.path.join(serverMilestonePath,
#                                                                                           usedServerPathForVersion,
#                                                                                           "Debug"))
#             # logger.info("DEBUG_BUILD_FOLDER: " + os.path.join(server_milestone_path, used_server_path_for_version, "Debug"))
#         except Exception as e:
#             self.logger.warning(
#                 "Exception raised when listing server paths for " + extractedCoreDump + " with version " + buildVersion + " The folder does not exist on " +
#                 serverInfo["server_name"] + " Exception thrown: " + repr(e))
#             return False
#
#         # normal builds
#         variantsFolders = [s for s in variantsFolders if "emergency" not in s]
#
#         # only Mainline builds
#         variantsFolders = [s for s in variantsFolders if "YoctoMainline" in s]
#
#         # gather all we have
#         foundFolder = "\n".join(s for s in variantsFolders if variant in s)
#
#         if not foundFolder:
#             self.logger.info("Could not found any build for " + extractedCoreDump + " with version " + buildVersion)
#             return False
#
#         serverDownloadDir = os.path.join(serverMilestonePath, usedServerPathForVersion, "Debug", foundFolder)
#         serverDownloadFile = "debugzip.tar.bz2"
#         serverFullDownloadPath = os.path.join(serverDownloadDir, serverDownloadFile)
#
#         relativeBuildPath = serverFullDownloadPath.split('.tar.bz2')[0]
#         isEmergency = True if 'emergency' in serverDownloadDir else False
#
#         if os.path.isfile(os.path.join(self.targetsLocation, serverFullDownloadPath)):
#             return True
#
#         # logger.info("WHERE TO DOWNLOAD BUILD: " + os.path.join(local_downloads_dir, server_download_dir))
#         self.logger.info("Begin downloading for " + extractedCoreDump + " with version " + buildVersion)
#         try:
#             fileObj = tempfile.NamedTemporaryFile()
#             file_attributes, filesize = conn.retrieveFile(serverRootPath, serverFullDownloadPath, fileObj)
#         except Exception as e:
#             self.logger.error(
#                 "Exception raised when downloading build for " + extractedCoreDump + " with version " + buildVersion + " Exception: " + repr(
#                     e))
#             return False
#         self.logger.info("Downloaded in " + serverFullDownloadPath)
#
#         MiscUtils.create_dir_if_not_exists(os.path.join(self.targetsLocation, serverDownloadDir))
#
#         localDownloadFilename = os.path.join(self.targetsLocation, serverDownloadDir, serverDownloadFile)
#
#         # Write it on disk
#         try:
#             fileObj.seek(0)
#             # logger.info("BUILD ZIP: " + local_download_filename)
#             g = open(localDownloadFilename, "wb")
#             g.write(fileObj.read())
#             fileObj.close()
#             g.close()
#         except Exception as e:
#             self.logger.error(
#                 "Exception raised when trying to write to disk the build for " + extractedCoreDump + " with version " + buildVersion + " Exception: " + str(
#                     e))
#             return False
#
#         # Now extract it
#         self.logger.info("Now attempt to extract..")
#         try:
#             if MiscUtils.is_os_windows():
#                 # since using tar from cygwin shell, properly convert paths to "/cigdrive/E/archive.tar.bz2"
#                 command = "tar -xvjf {archive_location} -C {out_path}".format(
#                     archive_location=MiscUtils.convert_in_cygwin_path(localDownloadFilename),
#                     out_path=os.path.join(MiscUtils.convert_in_cygwin_path(self.targetsLocation), serverDownloadDir))
#                 command = command.replace("\\", "/")
#                 MiscUtils.exec_cygwin_command(command)
#             else:
#                 command = "tar -xjvf {archive_location} -C {out_path}".format(archive_location=localDownloadFilename,
#                                                                               out_path=os.path.join(self.targetsLocation,
#                                                                                                     serverDownloadDir))
#                 MiscUtils.exec_os_command(command)
#         except Exception as e:
#             self.logger.error(
#                 "Exception raised when trying to extract the build for " + extractedCoreDump + " with version " + buildVersion + " Exception: " + str(
#                     e))
#             return False
#
#         self.logger.info("Extracted to " + localDownloadFilename.split(".")[0])

