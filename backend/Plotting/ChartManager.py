import LoggerFactory, time, jsonpickle, json
# import pandas as pd
from Singleton import Singleton
from ChartGenerator import *
from Globals import *
from TicketRepo import TicketRepo
from ChartCreationStatusResponse import ChartCreationStatusResponse

@Singleton
class ChartManager(object):

    def __init__(self):
        self.chartGenerator = ChartGenerator()
        self.logger = LoggerFactory.getFactoryLogger("ChartManager", WORKING_DIR, "server")

    def createDataDictBar(self, x, y, title, hoverinfo='label', orientation='h', annotations=[], width=700,
                       height=700):
        if height < 200:
            height = height*2
        elif height > 800:
            height = height/2

        data = dict()
        data['data'] = [{}]
        data['data'][0]['hoverinfo'] = hoverinfo
        data['data'][0]['orientation'] = orientation
        data['data'][0]['type'] = 'bar'

        data['layout'] = {}
        data['layout']['title'] = "<b>{title}</b>".format(title = title)
        data['layout']['annotations'] = annotations


        if orientation == 'h':
            data['data'][0]['x'] = x
            data['data'][0]['y'] = y
            data['layout']['width'] = width
            data['layout']['height'] = height
        else:
            data['data'][0]['x'] = y
            data['data'][0]['y'] = x
            data['layout']['width'] = height
            data['layout']['height'] = width

        return data

    def createDataDictPie(self, values, labels, title, hoverinfo = 'label+percent', hole = '0.5', type = 'pie',
                          width = 700, height = 700):
        data = dict()
        data['data'] = [{}]
        data['data'][0]['values'] = values
        data['data'][0]['labels'] = labels
        data['data'][0]['hoverinfo'] = hoverinfo
        data['data'][0]['type'] = type
        data['data'][0]['hole'] = hole

        data['layout'] = {}
        data['layout']['title'] = "<b>{title}</b>".format(title = title)
        data['layout']['width'] = width
        data['layout']['height'] = height

        return data

    # def resolveDataframeSeries(self, jsonData):
    #     df = pd.read_json(jsonData).generalInfo.to_json()
    #     gIFrame = pd.read_json(df)
    #     return gIFrame

    # def getLabelsForCustomChart(self, jsonData, gIFrame):
    def getLabelsForCustomChart(self, jsonData, tmpDictList):

        rawQueryDict = jsonData['rawQuery']
        for value in rawQueryDict.values():
            if not isinstance(value, list):
                return (None, None, None)

            else:
                for key, value2 in value[0].items():
                    labelField = key.split('.')[1]
                    if not isinstance(value2, dict):
                        return (labelField, [value2], False)

                    else:
                        for labelList in value2.values():
                            if isinstance(labelList, list):
                                return (labelField, labelList, False)

                            else:
                                realLabellist = []
                                # realLabellist.append(gIFrame.T[labelField])
                                for item in tmpDictList:
                                     realLabellist.append(item['generalInfo'][labelField])

                                # return (labelField, realLabellist[0], True)
                                return (labelField, realLabellist, True)


    def createChart(self, jsonData):
        queryResult = TicketRepo.Instance().findTicketsFromRawQuery(jsonData['rawQuery'])
        if queryResult == '[]':
            return jsonpickle.encode(ChartCreationStatusResponse(False, 'No such data!', None), unpicklable=False)

        selectedChartTemplate = jsonData['selectedChartTemplate']

        if selectedChartTemplate == 'Ticket by Status':
            return jsonpickle.encode(ChartCreationStatusResponse(True, '', self.createStatusChart(queryResult)),
                                     unpicklable=False)

        elif selectedChartTemplate == 'Tickets by Stability Type':
            return jsonpickle.encode(ChartCreationStatusResponse(True, '', self.createStabilityTypeChart(queryResult)),
                                     unpicklable=False)

        elif selectedChartTemplate == 'Ticket Duration':
            return jsonpickle.encode(ChartCreationStatusResponse(True, '', self.createDurationChart(queryResult)),
                                     unpicklable=False)

        elif selectedChartTemplate == 'Tickets by Planned Week':
            pass

        elif selectedChartTemplate == 'Duplicated Tickets':
            pass

        elif selectedChartTemplate == 'Custom':
            return jsonpickle.encode(ChartCreationStatusResponse(True, '', self.createCustomChart(jsonData, queryResult)),
                                     unpicklable=False)

    def createCustomChart(self, jsonData, queryResult):
        # gIFrame = self.resolveDataframeSeries(queryResult)
        tmpDictList = json.loads(queryResult)

        # targetLabel, labels, isDateQ = self.getLabelsForCustomChart(jsonData, gIFrame)
        targetLabel, labels, isDateQ = self.getLabelsForCustomChart(jsonData, tmpDictList)

        if (targetLabel, labels, isDateQ) == (None, None, None):
            return ''

        values = []
        # for label in labels:
        #     values.append(gIFrame.T[gIFrame.T[targetLabel] == label].count()[targetLabel])

        for label in labels:
            counter = 0
            for item in tmpDictList:
                if item['generalInfo'][targetLabel] == label:
                    counter = counter + 1
            values.append(counter)

        if isDateQ:
            newLabels = []
            for l in labels:
                if isinstance(l, (int, str)):
                    t = l
                else:
                    t = time.strftime("%Y.%m.%d", time.localtime(int(l['$date'] / 1000)))
                newLabels.append(t)
            labels = newLabels

        barWidth = len(labels)*100
        chartOrientation=jsonData['chartOrientation']
        chartTitle = jsonData['chartTitle']

        if jsonData['chartType'] == 'Bar':

            finalChartdata = self.createDataDictBar(x=values, y=labels, title=chartTitle, orientation=chartOrientation,
                                                    height=barWidth)

        else:
            finalChartdata = self.createDataDictPie(values=values, labels=labels, title=chartTitle)

        return self.chartGenerator.generateChartDivElement(finalChartdata)

    def createDurationChart(self, queryResult):
        # gIFrame = self.resolveDataframeSeries (queryResult)

        tmpDictList = json.loads(queryResult)

        values = []
        labels = ["Less than 10 days", "10 to 20 days", "20 to 30 days", "30 to 40 days", "40 to 60 days",
                  "60 to 100 days", "More than 100 days"]
        limits = [10, 20, 30, 40, 60, 100]
        limits2 = {10:0, 20:0, 30:0, 40:0, 60:0, 100:0}

        for idx, limit in enumerate(limits):
            if idx == 0:
                for item in tmpDictList:
                    if item['generalInfo']['Age'] < limit:
                        # counter = counter + 1
                        limits2[limit] = limits2[limit] + 1
            else:
                for item in tmpDictList:
                    if item['generalInfo']['Age'] >= limits[idx - 1] and  item['generalInfo']['Age'] <= limit:
                        limits2[limit] = limits2[limit] + 1

            for item in tmpDictList:
                if item['generalInfo']['Age'] >= 100:
                    limits2[100] = limits2[100] + 1

        for limit in limits:
            values.append(limits2[limit])

        # for idx, limit in enumerate(limits):
        #     if idx == 0:
        #         values.append(gIFrame.T[gIFrame.T.Age < limit].count()['Age'])
        #     else:
        #         values.append(gIFrame.T[(gIFrame.T.Age >= limits[idx - 1]) & (gIFrame.T.Age <= limit)].count()['Age'])
        # values.append(gIFrame.T[gIFrame.T.Age > limits[-1]].count()['Age'])

        title = 'Ticket duration (Days)'
        annot = [dict(x=xi, y=yi, text=str(xi), xanchor='left', yanchor='center', showarrow=False) for xi, yi in
                 zip(values, labels)]

        durationData = self.createDataDictBar(x=values, y=labels, title=title, orientation='v', annotations=annot)

        return self.chartGenerator.generateChartDivElement(durationData)

    def createStatusChart(self, queryResult):
        # gIFrame = self.resolveDataframeSeries(queryResult)
        tmpDictList = json.loads(queryResult)
        values = []
        labels = ['Categorizing', 'Reproduction', 'Processing', 'PreIntegrating', 'Integrating', 'Verifying',
                  'Concluding', 'Closed']

        for label in labels:
            # values.append(gIFrame.T[gIFrame.T['Status'] == label].count()['Status'])
            counter = 0
            for item in tmpDictList:
                if item['generalInfo']['Status'] == label:
                    counter = counter + 1
            values.append(counter)

        title = 'Tickets by Status'
        annot = [dict(x=xi, y=yi, text=str(xi), xanchor='left', yanchor='center', showarrow=False) for xi, yi in
                 zip(values, labels)]

        statusData = self.createDataDictBar(x=values, y=labels, title=title, annotations=annot)

        return self.chartGenerator.generateChartDivElement(statusData)


    def createStabilityTypeChart(self, queryResult):
        # gIFrame = self.resolveDataframeSeries(queryResult)
        tmpDictList = json.loads(queryResult)

        values = []
        labels = ['Stability', 'Hang', 'OutOfMemory', 'Performance', 'Reset']
        for label in labels:
            # values.append(gIFrame.T[gIFrame.T.Category == label].count()['Category'])
            counter = 0
            for item in tmpDictList:
                if item['generalInfo']['Category'] == label:
                    counter = counter + 1
            values.append(counter)

        title = "Tickets by Stability Type"
        stabilityData = self.createDataDictPie(values, labels, title)

        return self.chartGenerator.generateChartDivElement(stabilityData)

    def createPlannedWeekChart(self, jsonData):
        pass

    def createDuplicatesChart(self, jsonData):
        pass
