import plotly

class ChartGenerator(object):
    """description of class"""
    def __init__(self):
        pass

    def generateChartDivElement(self, data):
        return plotly.offline.plot(data, include_plotlyjs=False, output_type='div')
