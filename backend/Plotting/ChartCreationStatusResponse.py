class ChartCreationStatusResponse(object):
    def __init__(self, status, errorMessage, chart, **kwargs):
        self.status = status
        self.errorMessage = errorMessage
        self.chart = chart