from mongoengine import *
from CoredumpJobResultResponse import CoredumpJobResultResponse

class Coredump(Document):
    coreDumpId = StringField(default = "")
    crash = EmbeddedDocumentField(CoredumpJobResultResponse, default = None)
    isFinished = BooleanField(default = False)
    processFullPath = StringField(default="")
    variant = StringField(default="")
    buildVersion = StringField(default="")
    coredumpPath = StringField(default="")

    def getId(self):
        return str(self.id)


