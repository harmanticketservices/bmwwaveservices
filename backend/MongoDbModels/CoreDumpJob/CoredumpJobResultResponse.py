from mongoengine import *

class CoredumpJobResultResponse(EmbeddedDocument):

    payload = StringField(default = "")
    bt = StringField(default = "")
    success = BooleanField(default = True)
    errorMessage = StringField(default = "")