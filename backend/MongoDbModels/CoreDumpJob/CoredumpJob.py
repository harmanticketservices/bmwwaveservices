from mongoengine import *

class CoredumpJob(Document):

    coredumpJobId = StringField()
    isFinished = BooleanField()
    extendedBacktrace = BooleanField(default = False)
    processFullPath = StringField(default = "")
    variant = StringField(default = "")
    buildVersion = StringField(default = "")
    coredumpPath = StringField(default = "")

    def getId(self):
        return str(self.id)


