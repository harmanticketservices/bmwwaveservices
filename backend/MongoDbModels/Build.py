from mongoengine import *

class Build(Document):
    version = StringField(default = "")
    variantNumber = StringField(default = "")
    relativePath = StringField(default = "")
    isEmergency = BooleanField(default = False)
