from mongoengine import *
from flask_security import UserMixin
from Role import Role

class User(UserMixin, Document):
    name = StringField(default="")
    password = StringField(default="")
    email = EmailField(default=None)
    active = BooleanField(default=True)
    roles = ListField(ReferenceField(Role), default=[])



