from mongoengine import *

class TicketJob(Document):

    ticketId = StringField()
    isFinished = BooleanField()
    extendedBacktrace = BooleanField(default = False)
    processFullPath = StringField(default = "")
    variant = StringField(default = "")

    def getId(self):
        return str(self.id)


