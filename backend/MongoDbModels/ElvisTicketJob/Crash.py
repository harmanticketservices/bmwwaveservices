from mongoengine import *

class Crash(Document):
    crashId = StringField()
    tickets = ListField(StringField())

    def getId(self):
        return str(self.id)