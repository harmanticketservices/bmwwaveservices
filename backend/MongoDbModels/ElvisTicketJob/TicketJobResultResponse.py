from mongoengine import *

class TicketJobResultResponse(EmbeddedDocument):

    payload = StringField(default = "")
    bt = StringField(default = "")
    success = BooleanField(default = True)
    errorMessage = StringField(default = "")