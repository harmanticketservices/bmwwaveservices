from mongoengine import *

class TicketElvisInfo(EmbeddedDocument):

    Ticket = IntField(default = 0)
    Title = StringField(default = "")
    SoftwareVersion = StringField(default = "")
    Priority = StringField(default = "")
    Category = StringField(default = "")
    Domain = StringField(default = "")
    Responsible = StringField(default = "")
    Planned = IntField(default = 0)
    Status = StringField(default = "")
    Reported = DateTimeField(default = None)
    Categorizing = DateTimeField(default = None)
    Processing = DateTimeField(default = None)
    Age = IntField(default = 0)
    Developer = StringField(default = "")
  
    
    