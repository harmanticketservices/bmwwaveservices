from mongoengine import *
from TicketJobResultResponse import TicketJobResultResponse
from TicketElvisInfo import TicketElvisInfo
from Tag import Tag

class Ticket(Document):
    crashIds = ListField(StringField(), default = [])
    ticketId = StringField(default = "")
    solvedCrashes = EmbeddedDocumentListField(TicketJobResultResponse, default = [])
    generalInfo = EmbeddedDocumentField(TicketElvisInfo, default = None)
    areBacktracesSolved = BooleanField(default = False)
    backtracesErrorMessage = StringField(default = "")
    hasKernelPanic = BooleanField(default = False)
    tags = ListField(StringField(), default = [])
    ticketStatus = StringField(default='')

    def getId(self):
        return str(self.id)


